<?php
/*
Plugin Name: Hotspots with Comments
Version: 1.4.5
Plugin URI: 
Description: Visitor created hotspots on images
Author: De Eindbaas
Author URI: http://eindbaas.nl/products/

Hotspots with Comments
Copyright 2015  De Eindbaas

*/

if ( ! defined( 'EBSIH_FILE' ) ) {
	define( 'EBSIH_FILE', __FILE__ );
}

if ( ! defined( 'EBSIH_PATH' ) ) {
	define( 'EBSIH_PATH', plugin_dir_path( EBSIH_FILE ) );
}

if ( ! defined( 'EBSIH_URL' ) ) {
	define( 'EBSIH_URL', plugin_dir_url( EBSIH_FILE ) );
}

if ( ! defined( 'EBSIH_BASENAME' ) ) {
	define( 'EBSIH_BASENAME', plugin_basename( EBSIH_FILE ) );
}

define( 'EBSIH_VERSION', '1.4.5' );


/* ***************************** CLASS LOADING *************************** */

if (is_admin() ) {
	require_once EBSIH_PATH . '/admin/Admin.class.php';
	$GLOBALS['ebsih_admin'] = new EBSIH_Admin;
} else {
	require_once EBSIH_PATH . '/frontend/Frontend.class.php';
	$GLOBALS['ebsih_frontend'] = new EBSIH_Frontend;
}

require_once EBSIH_PATH . '/admin/PostTypeHotspot.class.php';
require_once EBSIH_PATH . '/admin/HotspotImage.class.php';
require_once EBSIH_PATH . '/admin/HotspotComment.class.php';
$GLOBALS['ebsih_posttype_hotspot'] 	= new EBSIH_PostTypeHotspot;