<?php
/**
 * @package Admin
 */

if ( ! class_exists( 'EBSIH_Frontend' ) ) {

	class EBSIH_Frontend {

		/**
		 * Class constructor
		 */
		function __construct() {
			add_action( 'wp_enqueue_scripts', array($this, 'enqueue_scripts'));

			add_shortcode('hotspotimage', array($this, 'shortcode_hotspot_image' ));
			add_filter( 'no_texturize_shortcodes', array($this, 'no_texturize_shortcodes') );

			add_action('comment_post', array($this, 'ajaxify_comments'),20, 2);
			add_filter('widget_text', 'do_shortcode');
		}

		function no_texturize_shortcodes($shortcodes){
		    $shortcodes[] = 'hotspotimage';
		    return $shortcodes;
		}
    
	    function ajaxify_comments($comment_ID, $comment_status){

	            if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
	           
	            //If AJAX Request Then
	                    switch($comment_status){
	                            case '0':
	                                    //notify moderator of unapproved comment
	                                    wp_notify_moderator($comment_ID);
	                            case '1':
	                            		//Approved comment
	                                    echo "success";
	                                    $commentdata = get_comment($comment_ID, ARRAY_A);
	                                    $post = get_post($commentdata['comment_post_ID']);

	                            break;
	                            default:
	                    }
	                    exit;
	            }
	    }

		function enqueue_scripts() {

			wp_enqueue_style( 'ebsih-jquery-style', 'https://code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css', false, EBSIH_VERSION, 'screen');

			wp_register_style( 'ebsih-font-awesome', plugins_url( '/css/font-awesome.css' , EBSIH_FILE ), false, EBSIH_VERSION, 'screen');
			wp_register_style( 'ebsih-qtip', plugins_url( '/css/jquery.qtip.min.css' , EBSIH_FILE ), false, EBSIH_VERSION, 'screen');
			wp_register_style( 'ebsih-hotspotimages', plugins_url( '/css/hotspotimages.css' , EBSIH_FILE ), false, EBSIH_VERSION, 'screen');
			wp_register_style( 'ebsih-style', plugins_url( '/css/screen.css' , EBSIH_FILE ), false, EBSIH_VERSION, 'screen');
			
			wp_register_script('ebsih-qtip-script', plugins_url('js/jquery.qtip.min.js', EBSIH_FILE), array( 'jquery'), EBSIH_VERSION, true );
			wp_register_script('ebsih-images-loaded-script', plugins_url('js/imagesloaded.pkgd.min.js', EBSIH_FILE), array( 'jquery'), EBSIH_VERSION, true );
			wp_register_script('ebsih-imagehotspot-script', plugins_url('js/imagehotspot.js', EBSIH_FILE), array( 'jquery'), EBSIH_VERSION, true );
		}

		function shortcode_hotspot_image( $atts ) {

		    extract( shortcode_atts( array(
		        'id' => 0,
		        'width' => '',
		        'height' => '400px',
		        'icon' => '',
		        'iconcolor' => '',
		        'iconsize' => '',
		    ), $atts ) );

		    if ((int)$id == 0) return '';

			if ( 'publish' == get_post_status ( (int)$id ) ) {
		   		$hotspotImage = new EBSIH_HotspotImage((int)$id);
				return $hotspotImage->getHotspotimage($width, $icon, $iconcolor, $iconsize);
			}
			else {
				return '';
			}
		}

	}
}