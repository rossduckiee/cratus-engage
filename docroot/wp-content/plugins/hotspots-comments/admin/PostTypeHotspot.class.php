<?php

if ( ! class_exists( 'EBSIH_PostTypeHotspot' ) ) {
	/**
	 * Class that holds most of the admin functionality
	 */
	class EBSIH_PostTypeHotspot {

		/**
		 * Class constructor
		 */
		function __construct() {
			add_action('init', array($this, 'register_post_type'));

			// disqus compatability
			if(has_action('pre_comment_on_post', 'dsq_pre_comment_on_post')) {
				remove_action('pre_comment_on_post', 'dsq_pre_comment_on_post');
				add_action('pre_comment_on_post', array($this, 'pre_comment_on_post'));
			}

			add_filter('manage_edit-ebsih_hotspot_columns', array($this, 'set_custom_edit_columns' ) );
			add_action('manage_ebsih_hotspot_posts_custom_column' , array($this, 'custom_column'), 10, 2 );

			add_action('add_meta_boxes', array($this,'add_meta_boxes')); 
			add_action('save_post', array($this, 'save_post'), 1, 2);
			add_action('comment_post', array($this,'comment_post'));

			add_filter('the_content', array($this, 'content_filter' ));
			add_filter('add_menu_classes', array($this, 'add_menu_classes'));
			add_filter('comment_post_redirect', array($this, 'comment_post_redirect') );
			add_filter('get_comment_text', array($this, 'get_comment_text'), 10, 2);
		}

		function register_post_type()
		{
			register_post_type('ebsih_hotspot',
				array(
					'labels' => array(
						'name' => __('Hotspot Images'),
						'singular_name' => __('Hotspot Image'),
						'add_new_item' => __('Add new Hotspot Image'),
						'edit_item' => __('Edit Hotspot Image'),
						'new_item' => __('New Hotspot Image'),
						'view_item' => __('View Hotspot Image'),
						'search_items' => __('Search Hotspots'),
						),
					'public' => true,
					'has_archive' => false,
					'menu_position' => 10,
					'supports' => array(
						'title',
						// 'thumbnail',
						'comments'
						),
					'rewrite' => array(
						'slug' => 'hotspot-image',
						),
						// 'register_meta_box_cb' => 'add_visuals_metaboxes'
					)
				);
		}

		// Duplicates Disqus function :: adds Hotspot Posttype exception
		function pre_comment_on_post($comment_post_ID) {
			if (dsq_can_replace() && get_post_type($comment_post_ID) != 'ebsih_hotspot') {
		        wp_die( dsq_i('Sorry, the built-in commenting system is disabled because Disqus is active.') );
		    }
		    return $comment_post_ID;
		}
	

		function set_custom_edit_columns($columns) {
		    $new_columns['hotspotimage-thumb'] = __( '', 'ebsih' );
		    // $new_columns['shortcode'] = __( 'shortcode', 'your_text_domain' );

			// insert new columns into position
			$position = 1;

		    $columns = array_merge(
        		array_slice($columns, 0, $position),
        		$new_columns,
        		array_slice($columns, $position)
    		);

    		$new_columns['shortcode'] = __( 'Shortcode', 'ebsih' );
    		$new_columns['public_hotspots'] = __( 'Visibility', 'ebsih' );
    		$new_columns['comments_open'] = __( 'Comments open', 'ebsih' );

    		$position = 3;
		    $columns = array_merge(
        		array_slice($columns, 0, $position),
        		$new_columns,
        		array_slice($columns, $position)
    		);
    		return $columns;

		}

		function custom_column( $column, $post_id ) {
			$hotspotImage = new EBSIH_HotspotImage($post_id);
		    switch ( $column ) {

		        case 'hotspotimage-thumb' :
		            echo '<a href="'. get_edit_post_link( $post_id ) .'"><img src="'. $hotspotImage->getImageURL() .'" width="60"  height="60" /></a>';
		            break;

		        case 'shortcode' :
		            echo $hotspotImage->getShortcode();
		            break;

		        case 'comments_open' :
		        	echo comments_open($post_id) ? "<i class='hotspot-icon-comment' style='font-size: 20px; color: #555;' title='Comments Open' />" : "<i class='hotspot-icon-ban-circle' style='font-size: 20px; color: #888;' title='Comments Closed' />";
		        	break;

		        case 'public_hotspots' :
		        	echo $hotspotImage->getSetting('pin_display') ? "<i class='hotspot-icon-eye-open' style='font-size: 20px; color: #555;' title='Public' />" : "<i class='hotspot-icon-eye-close' style='font-size: 20px; color: #888;' title='Private' />";

		        	break;

		    }
		}


		function add_meta_boxes() {
		    add_meta_box('ebsih_image', 'Image', array($this, 'meta_box_image'), 'ebsih_hotspot', 'normal');  
		    add_meta_box('ebsih_settings', 'Hotspot Image settings', array($this, 'meta_box_settings'), 'ebsih_hotspot', 'normal');  
		    add_meta_box('ebsih_shortcode', 'Shortcode', array($this, 'meta_box_shortcode'), 'ebsih_hotspot', 'side');  
		    remove_meta_box( 'slugdiv', 'ebsih_hotspot', 'normal' );
		}

		function meta_box_image($post) {
			$image_url = get_post_meta($post->ID, 'ebsih_image_url', true);
			$image_selected = get_post_meta($post->ID, 'ebsih_image_selected', true);

			$hotspotImage = new EBSIH_HotspotImage($post->ID);

			echo $hotspotImage->getHotspotimage();
			?>

				<a href="#" class="button" id="ebsih_upload_image_text">Select image</a>

				<input type="hidden" id="ebsih_image_url" name="ebsih_image_url" value="<?php echo $image_url; ?>" />
				<input type="hidden" id="ebsih_image_selected" name="ebsih_image_selected" value="<?php echo $image_selected; ?>" />
			<?php
		}

		function meta_box_settings($post) {
			$hotspotImage = new EBSIH_HotspotImage($post->ID);
			?>

			<table class="form-table ebsih-form-table">
				<tr><th colspan="2"><h3>Hotspots</h3></th></tr>
				<tr>
					<td>Icon</td>
					<td>
						<a id="hotspot-icon-thickbox-a" href="#TB_inline?width=600&height=550&inlineId=ebsih_pin_select_thickbox" class="thickbox" title="Choose Pin Icon">
							<i class="<?php echo $hotspotImage->getSetting('pin_icon'); ?>"></i>
						</a>
						<?php $hotspotImage->getIconSelectThickbox(); ?>	
						<input type="hidden" name="hotspotimage_settings[pin_icon]" id="hotspotimage_settings[pin_icon]" value="<?php echo $hotspotImage->getSetting('pin_icon'); ?>" />					
					</td>
				</tr>
				<tr>
					<td>Icon color</td>
					<td>
						<input type="text" id="hotspotimage_settings[pin_color]" name="hotspotimage_settings[pin_color]" value="<?php echo $hotspotImage->getSetting('pin_color'); ?>" />
					</td>
				</tr>
				<tr>
					<td>Icon size</td>
					<td>
						<select name="hotspotimage_settings[pin_size]" id="hotspotimage_settings[pin_size]">
							<?php for ($pin_size = 10; $pin_size < 101; $pin_size ++): ?>
							<option value="<?php echo $pin_size; ?>px" <?php if ($hotspotImage->getSetting('pin_size') == $pin_size) echo 'selected'; ?>><?php echo $pin_size; ?> px</option>
							<?php endfor; ?>
						</select>
					</td>
				</tr>

				<tr><th colspan="2"><h3>Pop-ups</h3></th></tr>
				<tr>
					<td>Pop-up style</td>
					<td class="ebsih-balloon-preview">
						<a id="hotspot-qtip-thickbox-a" href="#TB_inline?width=200&height=550&inlineId=ebsih_qtip_select_thickbox" class="thickbox" title="Choose Balloon Style">
							<div class="qtip <?php echo $hotspotImage->getSetting('qtip_style'); ?>" id="qtip-preview-balloon" >
								<div class="qtip-titlebar">
									<div aria-atomic="true" class="qtip-title" id="qtip-0-title">
										<span style="display: block; visibility: visible;" class="ebsih-bubble-author">
											Title
										</span>
									</div>
								</div>
								<div aria-atomic="true" id="qtip-0-content" class="qtip-content">
									Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
								</div>
							</div>
						</a><br />
						<?php $hotspotImage->getQtipSelectThickbox(); ?>
						<input type="hidden" name="hotspotimage_settings[qtip_style]" id="hotspotimage_settings[qtip_style]" value="<?php echo $hotspotImage->getSetting('qtip_style'); ?>" />					
					</td>
				</tr>
				<tr>
					<td>Pop-up width</td>
					<td>
						<input type="text" name="hotspotimage_settings[popup_width]" id="hotspotimage_settings[popup_width]" value="<?php echo $hotspotImage->getPopupWidth(); ?>" /> in pixels (ex: 280px)
						<p>Leave empty for auto-stretch</p>
					</td>
				</tr>
				<tr>
					<td>Pop-up position</td>
					<td>
						<select name="hotspotimage_settings[balloon_position]" id="hotspotimage_settings[balloon_position]">
							<option value="top" <?php if ($hotspotImage->getSetting('balloon_position') == 'top') echo 'selected'; ?>>Top</option>
							<option value="bottom" <?php if ($hotspotImage->getSetting('balloon_position') == 'bottom') echo 'selected'; ?> >Bottom</option>
							<option value="left" <?php if ($hotspotImage->getSetting('balloon_position') == 'left') echo 'selected'; ?>>Left</option>
							<option value="right" <?php if ($hotspotImage->getSetting('balloon_position') == 'right') echo 'selected'; ?>>Right</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>Show Pop-up on:</td>
					<td>
						<label title='Show balloon on mouse-hover'><input type='radio' name="hotspotimage_settings[balloon_show]" id="hotspotimage_settings[balloon_show]" value='hover' <?php if ($hotspotImage->getSetting('balloon_show') == 'hover') echo 'checked'; ?> /><span>hover</span></label>
						<label title='Show balloon on mouse-click'><input type='radio' name="hotspotimage_settings[balloon_show]" id="hotspotimage_settings[balloon_show]" value='click' <?php if ($hotspotImage->getSetting('balloon_show') == 'click') echo 'checked'; ?> /><span>click</span></label>
					</td>
				</tr>


				<tr><th colspan="2"><h3>General</h3></th></tr>
				<tr>
					<td><label for="hotspotimage_settings[pin_display]">Visibility</label></td>
					<td>
						<input type="checkbox" value="1" name="hotspotimage_settings[pin_display]" id="hotspotimage_settings[pin_display]" <?php if ($hotspotImage->getSetting('pin_display')) echo 'checked'; ?> /> Show hotspots on website
					</td>
				</tr>
				<tr>
					<td><label for="hotspotimage_settings[ajaxify_form]">Comment form</label></td>
					<td>
						<input type="checkbox" value="1" name="hotspotimage_settings[ajaxify_form]" id="hotspotimage_settings[ajaxify_form]" <?php if ($hotspotImage->getSetting('ajaxify_form')) echo 'checked'; ?> /> Use AJAX
					</td>
				</tr>
				<tr>
					<td></td>
					<td>
						<input name="submit" id="submit" class="button button-primary" value="Save settings" type="submit">
					</td>
				</tr>
			</table>

		<?php
		}

		function meta_box_shortcode($post) {
			$hotspotImage = new EBSIH_HotspotImage($post->ID);
			?>
				<input onfocus="this.select();" readonly="readonly" value='<?php echo $hotspotImage->getShortcode(); ?>' class="ebsih-shortcode" type="text">
				<p>Copy &amp; Paste this shortcode in a page or post to display the image</p>
			<?php
		}

		function save_post($post_id, $post) {
			if($post->post_type != 'ebsih_hotspot') return;

			$hotspotImage = new EBSIH_HotspotImage($post->ID);
			$hotspotImage->savePost();
		}

		function comment_post( $comment_id ) {

			// global $post not available in WP4.4 fix:
			$comment 	= get_comment($comment_id);
			$post_id 	= $comment->comment_post_ID;
			$post 		= get_post($post_id);

			// TODO :: add nonce
			if(!isset($post) || $post->post_type != 'ebsih_hotspot') return;

			if ( ( isset( $_POST['ebsih_hotspot_values'] ) ) && ( $_POST['ebsih_hotspot_values'] != '') ) {
				$json_object = json_decode( stripslashes($_POST['ebsih_hotspot_values']));

				// restrain percentages
				$json_object->x = (float)min( $json_object->x, 1);
				$json_object->y = (float)min( $json_object->y, 1);
				$json_object->x = (float)max( $json_object->x, 0);
				$json_object->y = (float)max( $json_object->y, 0);

				// update post meta array
				$hotspot_values['x'] = $json_object->x;
				$hotspot_values['y'] = $json_object->y;

				// save post meta array
				update_comment_meta( $comment_id, 'hotspot_values', $hotspot_values );
			}

		}

		// used for displaying a single hotspotimage page
		function content_filter($content) {
			global $post;
			if (empty($post) ) 							return $content;
			if ( $post->post_type != 'ebsih_hotspot' ) 	return $content;
			
			return do_shortcode('[hotspotimage id="'. $post->ID .'"]');
		}

		function get_comment_text($comment_content, $comment) {
			if ( !is_singular( 'ebsih_hotspot' ) ) return $comment_content;
			return $comment_content . "<br /><a class='ebsih_comment_hotspot_link' href='' data-comment-id='$comment->comment_ID'> Open hotspot</a>";
		}

		function add_menu_classes( $menu ) {
			global $post;

			// TODO :: total pending comments with parent == ebsih_hotspot
			$comments = get_comments( array(
				'status' => 'hold',
				'post_type' => 'ebsih_hotspot',
				'order' => 'ASC')
			);

			$pending_count = sizeof($comments);

		    // loop through $menu items, find match, add indicator
		    foreach( $menu as $menu_key => $menu_data ) {
		        if( 'edit.php?post_type=ebsih_hotspot' == $menu_data[2] )
		        	$menu[$menu_key][0] .= " <span class='update-plugins count-$pending_count'><span class='plugin-count'>$pending_count</span></span>";
		    }
		    return $menu;
		}


		function comment_post_redirect($location)
		{
			global $post;
			
			if ( $post->post_type != 'ebsih_hotspot' ) return $location;
			
			return $_SERVER["HTTP_REFERER"];
		}

	}
}