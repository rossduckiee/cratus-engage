<?php

if ( ! class_exists( 'EBSIH_HotspotComment' ) ) {

	class EBSIH_HotspotComment {

		public $comment = null;
		public $hotspotImage = null;

		private $pinIcon = '';
		private $pinColor = '';
		private $pinSize = '';
		private $popupStyle = '';
		private $popupWidth = '';
		private $popupPosition = '';

		function __construct($comment_id) {
			$this->comment = get_comment($comment_id);

			$this->hotspotImage = new EBSIH_HotspotImage($this->comment->comment_post_ID);

			$this->loadSettings();
		}

		private function loadSettings() {
			$settings = get_comment_meta($this->comment->comment_ID, 'ebsih_hotspot_settings', true);
			// echo "<pre>"; print_r($settings); die();
			
			if(isset($settings['pin_icon'])) $this->pinIcon = $settings['pin_icon'];
			if(isset($settings['pin_color'])) $this->pinColor = $settings['pin_color'];
			if(isset($settings['pin_size'])) $this->pinSize = $settings['pin_size'];
			if(isset($settings['qtip_style'])) $this->popupStyle = $settings['qtip_style'];
			if(isset($settings['popup_width'])) $this->popupWidth = $settings['popup_width'];
			if(isset($settings['popup_position'])) $this->popupPosition = $settings['popup_position'];
		}


		public function getSettings() {

			if (!is_array(get_comment_meta($this->comment->comment_ID, 'ebsih_hotspot_settings', true))) {
				// return default settings
				return $this->hotspotImage->getSettings();
			}
			else
			{
				return get_comment_meta($this->comment->comment_ID, 'ebsih_hotspot_settings', true);
			}
		}

		public function getSetting($setting_name){
			$settings = $this->getSettings();
			if (isset($settings[$setting_name]))
			{
				return $settings[$setting_name];
			}
			else
			{
				return $this->hotspotImage->getSetting($setting_name);
				// return false;
			}
		}

		public function getPinIcon() {
			if (!empty($this->pinIcon)) {
				return $this->pinIcon;	
			} else {
				return $this->hotspotImage->getSetting('pin_icon');;
			}
		}

		public function getPinColor() {
			if (!empty($this->pinColor)) {
				return $this->pinColor;	
			} else {
				return $this->hotspotImage->getPinColor();
			}
		}

		public function getPinSize() {

			if (!empty($this->pinSize)) {
				return $this->pinSize;	
			} else {
				return $this->hotspotImage->getPinSize();
			}
		}

		public function getPopupStyle() {
			if (!empty($this->popupStyle)) {
				return $this->popupStyle;	
			} else {
				return $this->hotspotImage->getPopupStyle();
			}
		}

		public function getPopupWidth() {
			if (!empty($this->popupWidth)) {
				return $this->popupWidth;	
			} else {
				return $this->hotspotImage->getPopupWidth();
			}
		}

		public function getPopupPosition() {

			if (!empty($this->popupPosition)) {
				return $this->popupPosition;	
			} else {
				return 'default';
			}
		}

		public function saveComment() {

			if ( isset( $_REQUEST['hotspotimage_settings'] ) ) {

				$settings = $_REQUEST['hotspotimage_settings'];
				
				// unset default icons
				if ( empty($settings['pin_icon']) || $settings['pin_icon'] == $this->hotspotImage->getPinIcon() ) unset($settings['pin_icon']);
				if ( $settings['pin_color'] == $this->hotspotImage->getPinColor() ) unset($settings['pin_color']);
				if ( $settings['pin_size'] == $this->hotspotImage->getPinSize() ) unset($settings['pin_size']);
				if ( $settings['qtip_style'] == $this->hotspotImage->getPopupStyle() ) unset($settings['qtip_style']);
				if ( empty($settings['popup_width']) || $settings['popup_width'] == $this->hotspotImage->getPopupWidth() ) unset($settings['popup_width']);
				if ( empty($settings['popup_position']) || $settings['popup_position'] == $this->hotspotImage->getPopupPosition() ) unset($settings['popup_position']);
				
				update_comment_meta( $this->comment->comment_ID, 'ebsih_hotspot_settings', $settings );
			}

		}

	}
}