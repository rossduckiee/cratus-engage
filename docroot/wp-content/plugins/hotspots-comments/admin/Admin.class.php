<?php
/**
 * @package Admin
 */

if ( ! class_exists( 'EBSIH_Admin' ) ) {
	/**
	 * Class for admin functionality
	 */
	class EBSIH_Admin {

		/**
		 * Class constructor
		 */
		function __construct() {
			add_action('admin_enqueue_scripts', array($this, 'admin_enqueue_scripts'));
			add_action('wp_print_scripts', array($this, 'wp_dequeue_scripts'), 100 );
			add_action('add_meta_boxes_comment', array($this, 'add_meta_boxes_comment'));
			add_action( 'edit_comment', array($this, 'edit_comment' ));
		}

		function admin_enqueue_scripts() {
			$screen = get_current_screen();

			// only load scripts on edit-posttype screen
 			if ($screen->id != 'ebsih_hotspot' && $screen->id != 'comment' && $screen->id != 'edit-ebsih_hotspot') return;	

			// media select pop-up
			wp_enqueue_media();

        	// Add the color picker css file      
        	wp_enqueue_style('wp-color-picker');
        	wp_enqueue_script('wp-color-picker');
			
			wp_enqueue_style('ebsih-font-awesome', plugins_url('css/font-awesome.css', EBSIH_FILE), false, EBSIH_VERSION, 'screen');
			wp_enqueue_style('ebsih-ebsih-qtip', plugins_url('css/jquery.qtip.min.css', EBSIH_FILE), false, EBSIH_VERSION, 'screen');
			wp_enqueue_style('ebsih-hotspotimages-style', plugins_url('css/hotspotimages.css', EBSIH_FILE), false, EBSIH_VERSION, 'screen');
			wp_enqueue_style('ebsih-admin-style', plugins_url('css/admin.css', EBSIH_FILE), false, EBSIH_VERSION, 'screen');

			$jquery_ui = array(
				"jquery-ui-widget",
				"jquery-ui-mouse",
				"jquery-ui-accordion",
				"jquery-ui-autocomplete",
				"jquery-ui-slider",
				"jquery-ui-tabs",
				"jquery-ui-sortable",	
				"jquery-ui-draggable",
				"jquery-ui-droppable",
				"jquery-ui-selectable",
				"jquery-ui-position",
				"jquery-ui-datepicker",
				"jquery-ui-resizable",
				"jquery-ui-dialog",
				"jquery-ui-button",
			);
			foreach($jquery_ui as $script){
				wp_enqueue_script($script);
			}

			wp_enqueue_script('ebsih-admin-qtip-script', plugins_url('js/jquery.qtip.min.js', EBSIH_FILE), array( 'jquery' ), EBSIH_VERSION, true );
			wp_enqueue_script('ebsih-admin-hotspotimages-script', plugins_url('js/imagehotspot.js', EBSIH_FILE), array( 'jquery' ), EBSIH_VERSION, true );
			wp_enqueue_script('ebsih-admin-script', plugins_url('js/admin.js', EBSIH_FILE), array( 'jquery' ), EBSIH_VERSION, true );
		}

 		function wp_dequeue_scripts() {
 			if (!is_admin()) return;

			$screen = get_current_screen();
 			if($screen->id == 'ebsih_hotspot') {
				wp_dequeue_script( 'jquery-qtip' );
 			}
 		}

		function add_meta_boxes_comment($comment) {
			// only show meta box for hotspot comments
			if ('ebsih_hotspot' == get_post_type( $comment->comment_post_ID )) {
				add_meta_box('ebsih_comment_popup', 'Hotspot settings', array($this, 'meta_box_hotspot_settings'), 'comment', 'normal');
			}
		}

		function meta_box_hotspot_settings($comment) {
			$hotspotImage = new EBSIH_HotspotImage($comment->comment_post_ID);
			$hotspotComment = new EBSIH_HotspotComment($comment->comment_ID);
			wp_nonce_field( 'ebsih_comment_update', 'ebsih_comment_update', false );
			?>


			<table class="form-table ebsih-form-table">

				<tr><th colspan="2">Icon</th></tr>
				<tr>
					<td>
						Icon
					</td>
					<td>
						<a id="hotspot-icon-thickbox-a" href="#TB_inline?width=600&height=550&inlineId=ebsih_pin_select_thickbox" class="thickbox" title="Choose Pin Icon">
							<i class="<?php echo $hotspotComment->getPinIcon(); ?>"></i>
						</a>
						<a href="#" id="ebish_comment_remove_icon" title="remove custom icon">[ set to default ]</a>
						<?php $hotspotImage->getIconSelectThickbox(); ?>	
						<input type="hidden" name="hotspotimage_settings[pin_icon]" id="hotspotimage_settings[pin_icon]" value="<?php echo $hotspotComment->getPinIcon(); ?>" />					
					</td>
				</tr>
				<tr>
					<td>Icon color</td>
					<td>
						<input type="text" id="hotspotimage_settings[pin_color]" name="hotspotimage_settings[pin_color]" value="<?php echo $hotspotComment->getPinColor(); ?>" />
					</td>
				</tr>
				<tr>
					<td>Icon size</td>
					<td>
						<select name="hotspotimage_settings[pin_size]" id="hotspotimage_settings[pin_size]">
							<?php for ($pin_size = 10; $pin_size < 101; $pin_size ++): ?>
							<option value="<?php echo $pin_size; ?>px" <?php if ($hotspotComment->getPinSize() == $pin_size) echo 'selected'; ?>><?php echo $pin_size; ?> px</option>
							<?php endfor; ?>
						</select>
					</td>
				</tr>

				<tr><th colspan="2">Pop-ups</th></tr>
				<tr>
					<td>Pop-up style</td>
					<td class="ebsih-balloon-preview">
						<a id="hotspot-qtip-thickbox-a" href="#TB_inline?width=200&height=550&inlineId=ebsih_qtip_select_thickbox" class="thickbox" title="Choose Popup Style">
							<div class="qtip <?php echo $hotspotComment->getPopupStyle(); ?>" id="qtip-preview-balloon" >
								<div class="qtip-titlebar">
									<div aria-atomic="true" class="qtip-title" id="qtip-0-title">
										<span style="display: block; visibility: visible;" class="ebsih-bubble-author">
											Title
										</span>
									</div>
								</div>
								<div aria-atomic="true" id="qtip-0-content" class="qtip-content">
									Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
								</div>
							</div>
						</a><br />
						<?php $hotspotImage->getQtipSelectThickbox(); ?>
						<input type="hidden" name="hotspotimage_settings[qtip_style]" id="hotspotimage_settings[qtip_style]" value="<?php echo $hotspotComment->getPopupStyle(); ?>" />
					</td>
				</tr>
				<tr>
					<td>Pop-up width</td>
					<td>
						<input type="text" name="hotspotimage_settings[popup_width]" id="hotspotimage_settings[popup_width]" value="<?php echo $hotspotComment->getPopupWidth(); ?>" /> in pixels (ex: 450px)
					</td>
				</tr>

				<tr>
					<td>Pop-up position :: <?php echo $hotspotComment->getPopupPosition(); ?></td>
					<td>
						<select name="hotspotimage_settings[popup_position]" id="hotspotimage_settings[popup_position]">
							<option value="default" <?php if ($hotspotComment->getPopupPosition() == 'default') echo 'selected'; ?>>Default</option>
							<option value="top" <?php if ($hotspotComment->getPopupPosition() == 'top') echo 'selected'; ?>>Top</option>
							<option value="bottom" <?php if ($hotspotComment->getPopupPosition() == 'bottom') echo 'selected'; ?> >Bottom</option>
							<option value="left" <?php if ($hotspotComment->getPopupPosition() == 'left') echo 'selected'; ?>>Left</option>
							<option value="right" <?php if ($hotspotComment->getPopupPosition() == 'right') echo 'selected'; ?>>Right</option>
						</select>
					</td>
				</tr>

			</table>

			<?php
		}


		function edit_comment( $comment_id )
		{
			// check comment post type
			if ('ebsih_hotspot' != get_post_type( get_comment($comment_id)->comment_post_ID )) return;
			
			// check nonce
		    if(!isset( $_POST['ebsih_comment_update'] ) || ! wp_verify_nonce( $_POST['ebsih_comment_update'], 'ebsih_comment_update' ) ) return;

		    $hotspotComment = new EBSIH_HotspotComment($comment_id);
			$hotspotComment->saveComment();

		}

	}
}