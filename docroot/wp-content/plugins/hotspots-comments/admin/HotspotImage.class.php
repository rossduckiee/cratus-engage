<?php

if ( ! class_exists( 'EBSIH_HotspotImage' ) ) {

	class EBSIH_HotspotImage {

		public $post = '';
		public $pinColor = '';
		public $pinIcon = '';
		public $pinSize = '';

		// public $fontawesomes = array('icon-glass','icon-music','icon-search','icon-envelope-alt','icon-heart','icon-star','icon-star-empty','icon-user','icon-film','icon-th-large','icon-th','icon-th-list','icon-ok','icon-remove','icon-zoom-in','icon-zoom-out','icon-power-off','icon-signal','icon-gear','icon-trash','icon-home','icon-file-alt','icon-time','icon-road','icon-download-alt','icon-download','icon-upload','icon-inbox','icon-play-circle','icon-rotate-right','icon-refresh','icon-list-alt','icon-lock','icon-flag','icon-headphones','icon-volume-off','icon-volume-down','icon-volume-up','icon-qrcode','icon-barcode','icon-tag','icon-tags','icon-book','icon-bookmark','icon-print','icon-camera','icon-font','icon-bold','icon-italic','icon-text-height','icon-text-width','icon-align-left','icon-align-center','icon-align-right','icon-align-justify','icon-list','icon-indent-left','icon-indent-right','icon-facetime-video','icon-picture','icon-pencil','icon-map-marker','icon-adjust','icon-tint','icon-edit','icon-share','icon-check','icon-move','icon-step-backward','icon-fast-backward','icon-backward','icon-play','icon-pause','icon-stop','icon-forward','icon-fast-forward','icon-step-forward','icon-eject','icon-chevron-left','icon-chevron-right','icon-plus-sign','icon-minus-sign','icon-remove-sign','icon-ok-sign','icon-question-sign','icon-info-sign','icon-screenshot','icon-remove-circle','icon-ok-circle','icon-ban-circle','icon-arrow-left','icon-arrow-right','icon-arrow-up','icon-arrow-down','icon-mail-forward','icon-resize-full','icon-resize-small','icon-plus','icon-minus','icon-asterisk','icon-exclamation-sign','icon-gift','icon-leaf','icon-fire','icon-eye-open','icon-eye-close','icon-warning-sign','icon-plane','icon-calendar','icon-random','icon-comment','icon-magnet','icon-chevron-up','icon-chevron-down','icon-retweet','icon-shopping-cart','icon-folder-close','icon-folder-open','icon-resize-vertical','icon-resize-horizontal','icon-bar-chart','icon-twitter-sign','icon-facebook-sign','icon-camera-retro','icon-key','icon-gears','icon-comments','icon-thumbs-up-alt','icon-thumbs-down-alt','icon-star-half','icon-heart-empty','icon-signout','icon-linkedin-sign','icon-pushpin','icon-external-link','icon-signin','icon-trophy','icon-github-sign','icon-upload-alt','icon-lemon','icon-phone','icon-unchecked','icon-bookmark-empty','icon-phone-sign','icon-twitter','icon-facebook','icon-github','icon-unlock','icon-credit-card','icon-rss','icon-hdd','icon-bullhorn','icon-bell','icon-certificate','icon-hand-right','icon-hand-left','icon-hand-up','icon-hand-down','icon-circle-arrow-left','icon-circle-arrow-right','icon-circle-arrow-up','icon-circle-arrow-down','icon-globe','icon-wrench','icon-tasks','icon-filter','icon-briefcase','icon-fullscreen','icon-group','icon-link','icon-cloud','icon-beaker','icon-cut','icon-copy','icon-paperclip','icon-save','icon-sign-blank','icon-reorder','icon-list-ul','icon-list-ol','icon-strikethrough','icon-underline','icon-table','icon-magic','icon-truck','icon-pinterest','icon-pinterest-sign','icon-google-plus-sign','icon-google-plus','icon-money','icon-caret-down','icon-caret-up','icon-caret-left','icon-caret-right','icon-columns','icon-sort','icon-sort-down','icon-sort-up','icon-envelope','icon-linkedin','icon-rotate-left','icon-legal','icon-dashboard','icon-comment-alt','icon-comments-alt','icon-bolt','icon-sitemap','icon-umbrella','icon-paste','icon-lightbulb','icon-exchange','icon-cloud-download','icon-cloud-upload','icon-user-md','icon-stethoscope','icon-suitcase','icon-bell-alt','icon-coffee','icon-food','icon-file-text-alt','icon-building','icon-hospital','icon-ambulance','icon-medkit','icon-fighter-jet','icon-beer','icon-h-sign','icon-plus-sign-alt','icon-double-angle-left','icon-double-angle-right','icon-double-angle-up','icon-double-angle-down','icon-angle-left','icon-angle-right','icon-angle-up','icon-angle-down','icon-desktop','icon-laptop','icon-tablet','icon-mobile-phone','icon-circle-blank','icon-quote-left','icon-quote-right','icon-spinner','icon-circle','icon-mail-reply','icon-github-alt','icon-folder-close-alt','icon-folder-open-alt','icon-expand-alt','icon-collapse-alt','icon-smile','icon-frown','icon-meh','icon-gamepad','icon-keyboard','icon-flag-alt','icon-flag-checkered','icon-terminal','icon-code','icon-reply-all','icon-mail-reply-all','icon-star-half-full','icon-location-arrow','icon-crop','icon-code-fork','icon-unlink','icon-question','icon-info','icon-exclamation','icon-superscript','icon-subscript','icon-eraser','icon-puzzle-piece','icon-microphone','icon-microphone-off','icon-shield','icon-calendar-empty','icon-fire-extinguisher','icon-rocket','icon-maxcdn','icon-chevron-sign-left','icon-chevron-sign-right','icon-chevron-sign-up','icon-chevron-sign-down','icon-html5','icon-css3','icon-anchor','icon-unlock-alt','icon-bullseye','icon-ellipsis-horizontal','icon-ellipsis-vertical','icon-rss-sign','icon-play-sign','icon-ticket','icon-minus-sign-alt','icon-check-minus','icon-level-up','icon-level-down','icon-check-sign','icon-edit-sign','icon-external-link-sign','icon-share-sign','icon-compass','icon-collapse','icon-collapse-top','icon-expand','icon-euro','icon-gbp','icon-dollar','icon-rupee','icon-yen','icon-renminbi','icon-won','icon-bitcoin','icon-file','icon-file-text','icon-sort-by-alphabet','icon-sort-by-alphabet-alt','icon-sort-by-attributes','icon-sort-by-attributes-alt','icon-sort-by-order','icon-sort-by-order-alt','icon-thumbs-up','icon-thumbs-down','icon-youtube-sign','icon-youtube','icon-xing','icon-xing-sign','icon-youtube-play','icon-dropbox','icon-stackexchange','icon-instagram','icon-flickr','icon-adn','icon-bitbucket','icon-bitbucket-sign','icon-tumblr','icon-tumblr-sign','icon-long-arrow-down','icon-long-arrow-up','icon-long-arrow-left','icon-long-arrow-right','icon-apple','icon-windows','icon-android','icon-linux','icon-dribbble','icon-skype','icon-foursquare','icon-trello','icon-female','icon-male','icon-gittip','icon-sun','icon-moon','icon-archive','icon-bug','icon-vk','icon-weibo','icon-renren');
		public $fontawesomes = array('hotspot-icon-glass','hotspot-icon-music','hotspot-icon-search','hotspot-icon-envelope-alt','hotspot-icon-heart','hotspot-icon-star','hotspot-icon-star-empty','hotspot-icon-user','hotspot-icon-film','hotspot-icon-th-large','hotspot-icon-th','hotspot-icon-th-list','hotspot-icon-ok','hotspot-icon-remove','hotspot-icon-zoom-in','hotspot-icon-zoom-out','hotspot-icon-power-off','hotspot-icon-signal','hotspot-icon-gear','hotspot-icon-trash','hotspot-icon-home','hotspot-icon-file-alt','hotspot-icon-time','hotspot-icon-road','hotspot-icon-download-alt','hotspot-icon-download','hotspot-icon-upload','hotspot-icon-inbox','hotspot-icon-play-circle','hotspot-icon-rotate-right','hotspot-icon-refresh','hotspot-icon-list-alt','hotspot-icon-lock','hotspot-icon-flag','hotspot-icon-headphones','hotspot-icon-volume-off','hotspot-icon-volume-down','hotspot-icon-volume-up','hotspot-icon-qrcode','hotspot-icon-barcode','hotspot-icon-tag','hotspot-icon-tags','hotspot-icon-book','hotspot-icon-bookmark','hotspot-icon-print','hotspot-icon-camera','hotspot-icon-font','hotspot-icon-bold','hotspot-icon-italic','hotspot-icon-text-height','hotspot-icon-text-width','hotspot-icon-align-left','hotspot-icon-align-center','hotspot-icon-align-right','hotspot-icon-align-justify','hotspot-icon-list','hotspot-icon-indent-left','hotspot-icon-indent-right','hotspot-icon-facetime-video','hotspot-icon-picture','hotspot-icon-pencil','hotspot-icon-map-marker','hotspot-icon-adjust','hotspot-icon-tint','hotspot-icon-edit','hotspot-icon-share','hotspot-icon-check','hotspot-icon-move','hotspot-icon-step-backward','hotspot-icon-fast-backward','hotspot-icon-backward','hotspot-icon-play','hotspot-icon-pause','hotspot-icon-stop','hotspot-icon-forward','hotspot-icon-fast-forward','hotspot-icon-step-forward','hotspot-icon-eject','hotspot-icon-chevron-left','hotspot-icon-chevron-right','hotspot-icon-plus-sign','hotspot-icon-minus-sign','hotspot-icon-remove-sign','hotspot-icon-ok-sign','hotspot-icon-question-sign','hotspot-icon-info-sign','hotspot-icon-screenshot','hotspot-icon-remove-circle','hotspot-icon-ok-circle','hotspot-icon-ban-circle','hotspot-icon-arrow-left','hotspot-icon-arrow-right','hotspot-icon-arrow-up','hotspot-icon-arrow-down','hotspot-icon-mail-forward','hotspot-icon-resize-full','hotspot-icon-resize-small','hotspot-icon-plus','hotspot-icon-minus','hotspot-icon-asterisk','hotspot-icon-exclamation-sign','hotspot-icon-gift','hotspot-icon-leaf','hotspot-icon-fire','hotspot-icon-eye-open','hotspot-icon-eye-close','hotspot-icon-warning-sign','hotspot-icon-plane','hotspot-icon-calendar','hotspot-icon-random','hotspot-icon-comment','hotspot-icon-magnet','hotspot-icon-chevron-up','hotspot-icon-chevron-down','hotspot-icon-retweet','hotspot-icon-shopping-cart','hotspot-icon-folder-close','hotspot-icon-folder-open','hotspot-icon-resize-vertical','hotspot-icon-resize-horizontal','hotspot-icon-bar-chart','hotspot-icon-twitter-sign','hotspot-icon-facebook-sign','hotspot-icon-camera-retro','hotspot-icon-key','hotspot-icon-gears','hotspot-icon-comments','hotspot-icon-thumbs-up-alt','hotspot-icon-thumbs-down-alt','hotspot-icon-star-half','hotspot-icon-heart-empty','hotspot-icon-signout','hotspot-icon-linkedin-sign','hotspot-icon-pushpin','hotspot-icon-external-link','hotspot-icon-signin','hotspot-icon-trophy','hotspot-icon-github-sign','hotspot-icon-upload-alt','hotspot-icon-lemon','hotspot-icon-phone','hotspot-icon-unchecked','hotspot-icon-bookmark-empty','hotspot-icon-phone-sign','hotspot-icon-twitter','hotspot-icon-facebook','hotspot-icon-github','hotspot-icon-unlock','hotspot-icon-credit-card','hotspot-icon-rss','hotspot-icon-hdd','hotspot-icon-bullhorn','hotspot-icon-bell','hotspot-icon-certificate','hotspot-icon-hand-right','hotspot-icon-hand-left','hotspot-icon-hand-up','hotspot-icon-hand-down','hotspot-icon-circle-arrow-left','hotspot-icon-circle-arrow-right','hotspot-icon-circle-arrow-up','hotspot-icon-circle-arrow-down','hotspot-icon-globe','hotspot-icon-wrench','hotspot-icon-tasks','hotspot-icon-filter','hotspot-icon-briefcase','hotspot-icon-fullscreen','hotspot-icon-group','hotspot-icon-link','hotspot-icon-cloud','hotspot-icon-beaker','hotspot-icon-cut','hotspot-icon-copy','hotspot-icon-paperclip','hotspot-icon-save','hotspot-icon-sign-blank','hotspot-icon-reorder','hotspot-icon-list-ul','hotspot-icon-list-ol','hotspot-icon-strikethrough','hotspot-icon-underline','hotspot-icon-table','hotspot-icon-magic','hotspot-icon-truck','hotspot-icon-pinterest','hotspot-icon-pinterest-sign','hotspot-icon-google-plus-sign','hotspot-icon-google-plus','hotspot-icon-money','hotspot-icon-caret-down','hotspot-icon-caret-up','hotspot-icon-caret-left','hotspot-icon-caret-right','hotspot-icon-columns','hotspot-icon-sort','hotspot-icon-sort-down','hotspot-icon-sort-up','hotspot-icon-envelope','hotspot-icon-linkedin','hotspot-icon-rotate-left','hotspot-icon-legal','hotspot-icon-dashboard','hotspot-icon-comment-alt','hotspot-icon-comments-alt','hotspot-icon-bolt','hotspot-icon-sitemap','hotspot-icon-umbrella','hotspot-icon-paste','hotspot-icon-lightbulb','hotspot-icon-exchange','hotspot-icon-cloud-download','hotspot-icon-cloud-upload','hotspot-icon-user-md','hotspot-icon-stethoscope','hotspot-icon-suitcase','hotspot-icon-bell-alt','hotspot-icon-coffee','hotspot-icon-food','hotspot-icon-file-text-alt','hotspot-icon-building','hotspot-icon-hospital','hotspot-icon-ambulance','hotspot-icon-medkit','hotspot-icon-fighter-jet','hotspot-icon-beer','hotspot-icon-h-sign','hotspot-icon-plus-sign-alt','hotspot-icon-double-angle-left','hotspot-icon-double-angle-right','hotspot-icon-double-angle-up','hotspot-icon-double-angle-down','hotspot-icon-angle-left','hotspot-icon-angle-right','hotspot-icon-angle-up','hotspot-icon-angle-down','hotspot-icon-desktop','hotspot-icon-laptop','hotspot-icon-tablet','hotspot-icon-mobile-phone','hotspot-icon-circle-blank','hotspot-icon-quote-left','hotspot-icon-quote-right','hotspot-icon-spinner','hotspot-icon-circle','hotspot-icon-mail-reply','hotspot-icon-github-alt','hotspot-icon-folder-close-alt','hotspot-icon-folder-open-alt','hotspot-icon-expand-alt','hotspot-icon-collapse-alt','hotspot-icon-smile','hotspot-icon-frown','hotspot-icon-meh','hotspot-icon-gamepad','hotspot-icon-keyboard','hotspot-icon-flag-alt','hotspot-icon-flag-checkered','hotspot-icon-terminal','hotspot-icon-code','hotspot-icon-reply-all','hotspot-icon-mail-reply-all','hotspot-icon-star-half-full','hotspot-icon-location-arrow','hotspot-icon-crop','hotspot-icon-code-fork','hotspot-icon-unlink','hotspot-icon-question','hotspot-icon-info','hotspot-icon-exclamation','hotspot-icon-superscript','hotspot-icon-subscript','hotspot-icon-eraser','hotspot-icon-puzzle-piece','hotspot-icon-microphone','hotspot-icon-microphone-off','hotspot-icon-shield','hotspot-icon-calendar-empty','hotspot-icon-fire-extinguisher','hotspot-icon-rocket','hotspot-icon-maxcdn','hotspot-icon-chevron-sign-left','hotspot-icon-chevron-sign-right','hotspot-icon-chevron-sign-up','hotspot-icon-chevron-sign-down','hotspot-icon-html5','hotspot-icon-css3','hotspot-icon-anchor','hotspot-icon-unlock-alt','hotspot-icon-bullseye','hotspot-icon-ellipsis-horizontal','hotspot-icon-ellipsis-vertical','hotspot-icon-rss-sign','hotspot-icon-play-sign','hotspot-icon-ticket','hotspot-icon-minus-sign-alt','hotspot-icon-check-minus','hotspot-icon-level-up','hotspot-icon-level-down','hotspot-icon-check-sign','hotspot-icon-edit-sign','hotspot-icon-external-link-sign','hotspot-icon-share-sign','hotspot-icon-compass','hotspot-icon-collapse','hotspot-icon-collapse-top','hotspot-icon-expand','hotspot-icon-euro','hotspot-icon-gbp','hotspot-icon-dollar','hotspot-icon-rupee','hotspot-icon-yen','hotspot-icon-renminbi','hotspot-icon-won','hotspot-icon-bitcoin','hotspot-icon-file','hotspot-icon-file-text','hotspot-icon-sort-by-alphabet','hotspot-icon-sort-by-alphabet-alt','hotspot-icon-sort-by-attributes','hotspot-icon-sort-by-attributes-alt','hotspot-icon-sort-by-order','hotspot-icon-sort-by-order-alt','hotspot-icon-thumbs-up','hotspot-icon-thumbs-down','hotspot-icon-youtube-sign','hotspot-icon-youtube','hotspot-icon-xing','hotspot-icon-xing-sign','hotspot-icon-youtube-play','hotspot-icon-dropbox','hotspot-icon-stackexchange','hotspot-icon-instagram','hotspot-icon-flickr','hotspot-icon-adn','hotspot-icon-bitbucket','hotspot-icon-bitbucket-sign','hotspot-icon-tumblr','hotspot-icon-tumblr-sign','hotspot-icon-long-arrow-down','hotspot-icon-long-arrow-up','hotspot-icon-long-arrow-left','hotspot-icon-long-arrow-right','hotspot-icon-apple','hotspot-icon-windows','hotspot-icon-android','hotspot-icon-linux','hotspot-icon-dribbble','hotspot-icon-skype','hotspot-icon-foursquare','hotspot-icon-trello','hotspot-icon-female','hotspot-icon-male','hotspot-icon-gittip','hotspot-icon-sun','hotspot-icon-moon','hotspot-icon-archive','hotspot-icon-bug','hotspot-icon-vk','hotspot-icon-weibo','hotspot-icon-renren');		public $qtipStyles = array('qtip-bootstrap', 'qtip-light', 'qtip-bootstrap ebsih-blue', 'qtip-blue', 'qtip-bootstrap ebsih-green', 'qtip-green', 'qtip-bootstrap ebsih-yellow', 'qtip-default', 'qtip-bootstrap ebsih-red', 'qtip-red', 'qtip-bootstrap ebsih-dark', 'qtip-dark');

		function __construct($post_id) {
			$this->post = get_post($post_id);
		}

		public function getShortcode() {
			return '[hotspotimage id="' . $this->post->ID.'"]';
		}

		public function getHotspotimage($width = '', $pinIcon = '', $pinColor = '', $pinSize = '') {
			static $ebsih_shortcode_counter = 0; 
			$ebsih_shortcode_counter++;
			$hotspotImageID = $this->post->ID . '-' . $ebsih_shortcode_counter;


			if ($pinIcon == '') $pinIcon = $this->getSetting('pin_icon');
			if ($pinColor == '') $pinColor = $this->getSetting('pin_color');
			if ($pinSize == '') $pinSize = $this->getSetting('pin_size');

			$this->pinIcon = $pinIcon;
			$this->pinColor = $pinColor;
			$this->pinSize = $pinSize;

			// include Javasripts
			wp_enqueue_style( 'ebsih-font-awesome' );
			wp_enqueue_style( 'ebsih-qtip' );
			wp_enqueue_style( 'ebsih-hotspotimages' );
			wp_enqueue_style( 'ebsih-style' );

			wp_enqueue_script( 'ebsih-qtip-script' );
			wp_enqueue_script( 'ebsih-images-loaded-script' );
			wp_enqueue_script( 'ebsih-imagehotspot-script' );

			ob_start();
			?>
			<div id="super-wrapper-<?php echo $hotspotImageID; ?>">
			<div style="<?php if (!empty($width)) echo "width: $width;"; ?>" id="ebsih-hotspot-wrapper-<?php echo $hotspotImageID; ?>" class="ebsih-hotspot-wrapper" data-hotspot-image-id="<?php echo $hotspotImageID; ?>" data-pin-icon="<?php echo $this->pinIcon; ?>" data-pin-color="<?php echo $this->pinColor; ?>" data-qtip-style="<?php echo $this->getSetting('qtip_style'); ?>" data-balloon-position="<?php echo $this->getSetting('balloon_position'); ?>" data-balloon-show="<?php echo $this->getSetting('balloon_show'); ?>" data-ajaxify-form="<?php echo $this->getSetting('ajaxify_form'); ?>" >
		   		<div id="ebsih-hotspot-image-wrapper-<?php echo $hotspotImageID; ?>" class="ebsih-hotspot-image-wrapper <?php if (comments_open($this->post->ID) || is_admin()) echo "ebsih-image-comments-open"; ?>" >
		   			<img id="ebsih-image-<?php echo $hotspotImageID; ?>" src="<?php echo $this->getImageUrl(); ?>" alt="" />
		   			<div id="ensih-highlight-cursor-<?php echo $hotspotImageID; ?>" class="ensih-highlight-cursor <?php echo $this->pinIcon; ?>" style="font-size: <?php echo $this->pinSize; ?>; color: <?php echo $this->pinColor; ?>;" data-comments-open="<?php echo (comments_open($this->post->ID) || is_admin()); ?>" ></div>
		   			<div class="ebsih-notification ebsih-success">Notification 2</div>
		   			<?php echo $this->getHotspots(); ?>
		   			<?php if (is_admin()): ?>
						<div id="ebsih-admin-create-hotspot-tooltip" style="display: none;">
							<a class="ebsih-admin-create-hotspot-link" href="#abc" title="Create new hotspot" >Create new hotspot</a>
						</div>
		   			<?php endif; ?>
		   		</div>
		   		
		   		<?php if (!is_admin()): ?>
				<div class="ebsih-hotspot-comment-box-wrapper">
			   		<div id="ebsih-hotspot-comment-box-<?php echo $hotspotImageID; ?>" class="ebsih-hotspot-comment-box">
			   			<a href="#" class="close-hotspot-form" style="font-size: 20px; padding: 10px 20px; color: #666; float: right; text-decoration: none;"><i class="hotspot-icon-remove" style=""></i></a>

			   			<?php
			   			comment_form(array(
			   				'comment_notes_after' => '<input type="text" value="" id="ebsih-hotspot-values-' . $hotspotImageID . '" name="ebsih_hotspot_values" class="ebsih_hotspot_field" />',
			   				'title_reply' => '',
			   				'label_submit' => 'create hotspot',
			   			),
			   			$this->post->ID);
			   			?>
			   		</div>
		   		</div>
		   		<?php endif; ?>
		   	</div>
		   	</div>
		   	<p></p>
			<?php
			return ob_get_clean();
		}

		public function getHotspots() {
			global $wp_query, $withcomments, $post, $wpdb, $id, $comment, $user_login, $user_ID, $user_identity, $overridden_cpage;
			
			static $ebsih_shortcode_comment_counter = 0; 
			$ebsih_shortcode_comment_counter++;

			// Comment author information fetched from the comment cookies.
			$commenter = wp_get_current_commenter();
			$comment_author = $commenter['comment_author'];
			$comment_author_email = $commenter['comment_author_email'];

			if ( !$this->getSetting('pin_display') && !is_admin()) return '';

			if(is_admin()) {
				// get ALL comments
				$comments = get_comments(array(
					'post_id' => $this->post->ID,
					'post_type' => "ebsih_hotspot",
					'status' => 'all',
				));

			}
			else {

				if ( empty($comment_author) ) {
					// get approved comments
					$comments = get_comments( array('post_id' => $this->post->ID, 'status' => 'approve', 'order' => 'ASC') );
				} else {
					// get approved comments AND pending comments for current commenter
					$comments = $wpdb->get_results($wpdb->prepare("SELECT * FROM $wpdb->comments WHERE comment_post_ID = %d AND ( comment_approved = '1' OR ( comment_author = %s AND comment_author_email = %s AND comment_approved = '0' ) ) ORDER BY comment_date_gmt", $this->post->ID, wp_specialchars_decode($comment_author,ENT_QUOTES), $comment_author_email));
				}
			}


			ob_start();

			foreach($comments as $comment):
				$hotspotComment = new EBSIH_HotspotComment($comment->comment_ID);
				$hotspotCommentID = $comment->comment_ID . '-' . $ebsih_shortcode_comment_counter;
				$hostspot_values = get_comment_meta( $comment->comment_ID, 'hotspot_values', true );

				// don't show pins without coords
				if ( !isset($hostspot_values['x']) ) continue;
				if ( !isset($hostspot_values['y']) ) continue;
				
				?>

				<div id="ebsih-hotspot-pin-<?php echo $hotspotCommentID; ?>" class="ebsih-hotspot-pin <?php if (!$comment->comment_approved) echo 'ebsih-hotspot-pending' ?>" style="color: <?php echo $hotspotComment->getPinColor(); ?>;" data-comment-id="<?php echo $comment->comment_ID; ?>" data-hotspot-id="<?php echo $hotspotCommentID; ?>" data-x="<?php echo $hostspot_values['x']; ?>" data-y="<?php echo $hostspot_values['y']; ?>" >
					<i class="<?php echo $hotspotComment->getPinIcon(); ?>" style="font-size: <?php echo $hotspotComment->getPinSize(); ?>;"></i>
				</div>

				<div id="ebsih-hotspot-balloon-<?php echo $hotspotCommentID; ?>" class="ebsih-bubble" data-hotspot-id="<?php echo $hotspotCommentID; ?>" data-hotspot-author="<?php echo $comment->comment_author; ?>" data-hotspot-author-url="<?php echo $comment->comment_author_url; ?>"  data-popup-style="<?php echo $hotspotComment->getPopupStyle(); ?>" data-popup-width="<?php echo $hotspotComment->getPopupWidth(); ?>" data-popup-position="<?php echo $hotspotComment->getPopupPosition(); ?>" >
					<?php echo $comment->comment_content; ?>

					<?php if( current_user_can( 'moderate_comments' ) && is_admin() ): ?>
						<?php
							$del_nonce = esc_html( '_wpnonce=' . wp_create_nonce( "delete-comment_$comment->comment_ID" ) );
							$approve_nonce = esc_html( '_wpnonce=' . wp_create_nonce( "approve-comment_$comment->comment_ID" ) );
							$url = "comment.php?c=$comment->comment_ID";
							$approve_url = esc_url( $url . "&action=approvecomment&$approve_nonce" );
							$unapprove_url = esc_url( $url . "&action=unapprovecomment&$approve_nonce" );
							$spam_url = esc_url( $url . "&action=spamcomment&$del_nonce" );
							$trash_url = esc_url( $url . "&action=trashcomment&$del_nonce" );
							$delete_url = esc_url( $url . "&action=deletecomment&$del_nonce" );

							$the_comment_status = wp_get_comment_status( $comment->comment_ID );
						?>
						<div class="ebsih-balloon-edit">
							<a style='<?php if ('approved' == $the_comment_status) echo 'display: inline'; ?>' class='ebish_edit_comment_link ebish_unapprove_comment_link' href='<?php echo $unapprove_url; ?>' data-ebsih-comment-id='<?php echo $hotspotCommentID; ?>' data-wp-lists='delete:the-comment-list:comment-<?php echo $comment->comment_ID; ?>:e7e7d3:action=dim-comment&amp;new=unapproved' class='vim-u vim-destructive' title='Unapprove this comment'>Unapprove</a>
							<a style='<?php if ('unapproved' == $the_comment_status) echo 'display: inline'; ?>' class='ebish_edit_comment_link ebish_approve_comment_link' href='<?php echo  $approve_url; ?>' data-ebsih-comment-id='<?php echo $hotspotCommentID; ?>' data-wp-lists='delete:the-comment-list:comment-<?php echo  $comment->comment_ID; ?>:e7e7d3:action=dim-comment&amp;new=approved' class='vim-a vim-destructive' title='Approve this comment'>Approve</a>
							| <a class='ebish_edit_comment_link' href="<?php echo get_edit_comment_link( $comment->comment_ID); ?>" title='Edit comment' >Edit</a>
							| <a class="ebish_edit_comment_link ebish_trash_comment_link" href='<?php echo $spam_url; ?>' data-ebsih-comment-id='<?php echo $hotspotCommentID; ?>' data-wp-lists='delete:the-comment-list:comment-<?php echo $comment->comment_ID; ?>::spam=1' class='vim-s vim-destructive' title='Mark this comment as spam'>Spam</a>
							| <a class="ebish_edit_comment_link ebish_trash_comment_link" href='<?php echo $trash_url; ?>' data-ebsih-comment-id='<?php echo $hotspotCommentID; ?>' data-wp-lists='delete:the-comment-list:comment-<?php echo $comment->comment_ID; ?>::trash=1' class='delete vim-d vim-destructive' title='Move this comment to the trash'>Trash</a>
						</div>
					<?php endif; ?>
				</div>
				<?php
			endforeach;

			return ob_get_clean();
		}

		public function getComments() {
			$comments = get_comments(array(
				'post_id' => $this->post->ID,
			));

			foreach($comments as $comment) :
				// print_r($comment); die();
				echo $comment->comment_author;
				echo $comment->comment_content;
				print_r(get_comment_meta( $comment->comment_ID, 'hotspot_values', true ));
				echo "<br />";

			endforeach;
		}

		public function removeComment() {
 			wp_set_comment_status( $comment_id, $comment_status );
		}

		public function getImageURL() {
			return get_post_meta($this->post->ID, 'ebsih_image_url', true);
			// $image_selected = get_post_meta($post->ID, 'ebsih_image_selected', true);
		}

		public function getSettings() {

			if (!is_array(get_post_meta($this->post->ID, 'ebsih_settings', true))) {
				// return default settings
				return array(
					'pin_display' => '1',
					'pin_icon' => 'hotspot-icon-circle-blank',
					'pin_color' => '#dd3333',
					'pin_size' => '24px',
					'qtip_style' => 'qtip-bootstrap',
					'popup_width' => '280px',
					'balloon_position' => 'top',
					'balloon_show' => 'hover',
					'ajaxify_form' => '0',
				);
			}
			else
			{
				return get_post_meta($this->post->ID, 'ebsih_settings', true);
			}
		}

		public function getSetting($setting_name){
			$settings = $this->getSettings();
			if (isset($settings[$setting_name]))
			{
				return $settings[$setting_name];
			}
			else
			{
				return false;
			}
		}

		public function getPinIcon() {
			return $this->getSetting('pin_icon');
		}
		public function getPinColor() {
			return $this->getSetting('pin_color');
		}
		public function getPinSize() {
			return $this->getSetting('pin_size');
		}
		public function getPopupStyle() {
			return $this->getSetting('qtip_style');
		}
		public function getPopupWidth() {
			return $this->getSetting('popup_width');
		}
		public function getPopupPosition() {
			return $this->getSetting('balloon_show');
		}


		public function saveSettings() {

		}

		public function getIconSelectThickbox() {
			add_thickbox();
		?>
			<div id="ebsih_pin_select_thickbox" style="display:none;">
				<div id="hotspotimage_pin_selector" class="">
					<ul>
						<?php foreach ($this->fontawesomes as $icon_name): ?>
						<li>
							<a href="#" data-pin-icon="<?php echo $icon_name; ?>" title="<?php echo $icon_name; ?>">
								<i class="<?php echo $icon_name; ?>"></i>
							</a>
						</li>
						<?php endforeach; ?>
					</ul>
					<div class="clear"></div>
				</div>
			</div>	
		<?php
		}

		public function getQtipSelectThickbox() {
			add_thickbox();
		
		?>
			<div id="ebsih_qtip_select_thickbox" style="display:none;">
				<div id="hotspotimage_qtip_selector" class="">
					<?php foreach ($this->qtipStyles as $style): ?>
					
					<div class="qtip-preview-container" data-style="<?php echo $style; ?>"</div>
						<div style="z-index: 15002; display: block;" class="qtip qtip-default <?php echo $style; ?>">

							<div class="qtip-titlebar">
								<div aria-atomic="true" class="qtip-title" id="qtip-0-title">
									Title
								</div>
							</div>
							<div aria-atomic="true" id="qtip-0-content" class="qtip-content">
								Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
							</div>
						</div>
					</div>
					<?php endforeach; ?>
				</div>
			</div>

		<?php
		}

		public function savePost() {
			global $post;

			
			if ( isset( $_REQUEST['new_hotspot_position'] ) ) {
				// create net hotspot
				$json_object = json_decode(stripslashes($_REQUEST['new_hotspot_position']));
				// restrain percentages
				$json_object->x = (float)min( $json_object->x, 1);
				$json_object->y = (float)min( $json_object->y, 1);
				$json_object->x = (float)max( $json_object->x, 0);
				$json_object->y = (float)max( $json_object->y, 0);


				// create new comment
				$current_user = wp_get_current_user();
				// print_r($current_user); die();

				$commentdata = array(
					'comment_post_ID' => $post->ID,
					'comment_author' => $current_user->user_login, 
					'comment_author_email' => '', 
					'comment_author_url' => '', 
					'comment_content' => 'New hotspot created at ' . date('Y-m-d @ i:s '),
					'comment_type' => '',
					'comment_parent' => '',
					'user_id' => $current_user->ID,
				);

				// create new comment
				$comment_id = wp_new_comment($commentdata);

				// // update post meta array
				$hotspot_values['x'] = $json_object->x;
				$hotspot_values['y'] = $json_object->y;
				update_comment_meta( (int)$comment_id, 'hotspot_values', $hotspot_values );

			}
			elseif ( isset( $_REQUEST['hotspot_position_updates'] ) ) {
				// update dragged comment positions
				foreach ($_REQUEST['hotspot_position_updates'] as $comment_id => $position) {
					$json_object = json_decode(stripslashes($position));
					// restrain percentages
					$json_object->x = (float)min( $json_object->x, 1);
					$json_object->y = (float)min( $json_object->y, 1);
					$json_object->x = (float)max( $json_object->x, 0);
					$json_object->y = (float)max( $json_object->y, 0);

					// // update post meta array
					$hotspot_values['x'] = $json_object->x;
					$hotspot_values['y'] = $json_object->y;
					update_comment_meta( (int)$comment_id, 'hotspot_values', $hotspot_values );
				}
			}

			// update settings
			if ( isset( $_REQUEST['hotspotimage_settings'] ) ) {
				$settings = $_REQUEST['hotspotimage_settings'];
				update_post_meta($this->post->ID, 'ebsih_settings', $settings);
			}

		    if ( isset( $_REQUEST['ebsih_image_url'] ) ) {
		        update_post_meta( $this->post->ID, 'ebsih_image_url', sanitize_text_field( $_REQUEST['ebsih_image_url'] ) );
		    }
		    if ( isset( $_REQUEST['ebsih_image_selected'] ) ) {
		        update_post_meta( $this->post->ID, 'ebsih_image_selected', sanitize_text_field( $_REQUEST['ebsih_image_selected'] ) );
		    }
		}

	}
}