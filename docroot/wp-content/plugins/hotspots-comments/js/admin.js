jQuery(document).ready(function($){

    // alert(window.location.hash);
 
	var ebsih_uploader;
 
    $('#ebsih_upload_image_text').click(function(e) {
 
        // Prevents the default action from occuring.
        e.preventDefault();
 
        //If the uploader object has already been created, reopen the dialog
        if (ebsih_uploader) {
            // wp.media.editor.open;
            ebsih_uploader.open();
            return;
        }
 
        //Extend the wp.media object
        ebsih_uploader = wp.media.frames.file_frame = wp.media({
            title: 'Select Image',
            button: { text: 'Select Image' },
            library: { type: 'image' },
            // allowLocalEdits: true,

            multiple: false
        });
 
        //When a file is selected, grab the URL and set it as the text field's value
        ebsih_uploader.on('select', function() {
            attachment = ebsih_uploader.state().get('selection').first().toJSON();

            console.log(attachment);

            $('#ebsih_image_url').val(attachment.url);
            $('#ebsih_image_selected').val('true');

            // $("#ebsih_image_container").html("<img src='"+attachment.url+"'/>");
            $(".ebsih-hotspot-image-wrapper img").attr("src", attachment.url);

            $("#ebsih_upload_image_text").text("Choose image");


        });
 
        //Open the uploader dialog
        ebsih_uploader.open();

     });

    // create new hotspot from admin
    $('.ensih-highlight-cursor').qtip({ // apply tooltip
        content: {
            text: $('#ebsih-admin-create-hotspot-tooltip').html()
        },
        position: {
            my: 'bottom center',
            at: 'top center',
            viewport: jQuery(window),
        },
        // show: 'click',
        hide: 'unfocus'
    });

    $( document ).on( "click", ".ebsih-admin-create-hotspot-link", function() {


        // create new_hotspot_position[x, y]

        // submit form

        // get new x & y positions
        var cursor = $('.ensih-highlight-cursor');
        var position = cursor.position();
        var image = $('.ebsih-hotspot-image-wrapper img');
        // get center of pin
        position.left += cursor.width() / 2;
        position.top += cursor.height() / 2;
        // calculate percentage
        position.x = position.left / image.width();
        position.y = position.top / image.height();
        // console.log( "x: " + position.x + ", y: " + position.y );

        

        $("#new_hotspot_position").remove();
        new_element = $("<input type='text' id='new_hotspot_position' name='new_hotspot_position' value='new' />");
        new_element.val('{"x":'+position.x+', "y":'+position.y+'}');
        $('.ebsih-form-table').append(new_element);

        $( "#publish" ).click()

        return false;

    });


    // dragable icons
    $( ".ebsih-hotspot-pin" ).draggable({
        containment: ".ebsih-hotspot-image-wrapper img",
        start: function() {
            $('.ebsih-qtip').qtip('hide');
            $('.qtip').qtip('disable');
        },
        drag: function() {
        },
        stop: function() {
            $('.qtip').qtip('enable');

            // get new x & y positions
            var position = $(this).position();
            var image = $('.ebsih-hotspot-image-wrapper img');

            // get center of pin
            position.left += $(this).width() / 2;
            position.top += $(this).height() / 2;

            // calculate percentage
            position.x = position.left / image.width();
            position.y = position.top / image.height();
           
            // create or update hotspot_position_updates['hotspot element']['comment_id'][positions]
            // console.log( "x: " + position.x + ", y: " + position.y );
            var comment_id = $(this).data('comment-id');

            $("#hotspot_position_updates\\["+ comment_id+"\\]").remove();
            update_element = $("<input type='hidden' id='hotspot_position_updates["+ comment_id+"]' name='hotspot_position_updates["+ comment_id+"]' value='update' />");
            update_element.val('{"x":'+position.x+', "y":'+position.y+'}');
            $('.ebsih-form-table').append(update_element);
        }
    });

 // color picker

    var colorPickerOptions = {
        // a callback to fire whenever the color changes to a valid color
        color: "#00f",
        // change: function(event, ui){

        //     setTimeout(function() {
        //         $('#hotspot-icon-slected').css({color: $('#hotspotimage_settings\\[pin_color\\]').val() });
        //     }, 1000);
            
        //     // $('#hotspot-icon-slected').css({"background-color": $(this).val() });
        // },
        // a callback to fire when the input is emptied or an invalid color
        clear: function() {},
        mode: "hsv",
        // hide the color picker controls on load
        hide: true,
        // show a group of common colors beneath the square
        // or, supply an array of colors to customize further
        palettes: true
    };

    $('#hotspotimage_settings\\[pin_color\\]').wpColorPicker(colorPickerOptions);


    // pin select thickbox:
    $('#hotspotimage_pin_selector a').click(function(){
        var icon_name = $(this).data("pin-icon");

        jQuery('#hotspot-icon-thickbox-a i').attr("class", icon_name);
        
        $('#hotspotimage_settings\\[pin_icon\\]').val(icon_name);
        $('#ebsih_hotspot_settings\\[pin_icon\\]').val(icon_name);
        self.parent.tb_remove();
        return false;
    });

    // $( "#slider" ).slider();


    // qtip select thickbox: 
    $('div.qtip-preview-container').click(function(){
        var qtip_style = $(this).data("style");

        jQuery('#qtip-preview-balloon').attr("class", "qtip qtip-default " + qtip_style);
        $('#hotspotimage_settings\\[qtip_style\\]').val(qtip_style);

        if(qtip_style == 'qtip-bootstrap') {
            console.log('custom!');
        }

        self.parent.tb_remove();
        return false;
    });

    $('#hotspotimage_settings\\[qtip_rounded\\]').click(function(){
        jQuery('#qtip-preview-balloon').toggleClass('qtip-rounded');
    });
    $('#hotspotimage_settings\\[qtip_shadow\\]').click(function(){
        jQuery('#qtip-preview-balloon').toggleClass('qtip-shadow');
    });

    // ajax edit links in balloons
    jQuery( document ).on( "click", ".ebish_trash_comment_link", function() {
        var comment_id = jQuery(this).data('ebsih-comment-id');
        jQuery("#ebsih-hotspot-pin-" + comment_id).fadeOut();
        jQuery(this).closest('.qtip').hide();
        return false;
    });

    jQuery( document ).on( "click", ".ebish_approve_comment_link", function() {
        var comment_id = jQuery(this).data('ebsih-comment-id');
        jQuery("#ebsih-hotspot-pin-" + comment_id).removeClass('ebsih-hotspot-pending');
        jQuery(this).hide();
        jQuery(this).siblings('.ebish_unapprove_comment_link').css( "display", "inline");
        // jQuery(this).closest('.qtip').hide();
        return false;
    });

    jQuery( document ).on( "click", ".ebish_unapprove_comment_link", function() {
        var comment_id = jQuery(this).data('ebsih-comment-id');
        jQuery("#ebsih-hotspot-pin-" + comment_id).addClass('ebsih-hotspot-pending');
        jQuery(this).hide();
        jQuery(this).siblings('.ebish_approve_comment_link').css( "display", "inline");
        // jQuery(this).closest('.qtip').hide();
        return false;
    });


    // // update pin status after ajax comment-edit in posttype view
    // jQuery(document).ajaxSuccess(function(event, request, settings) {
    //     if(null != settings.element) {
    //         var splitted = settings.element.split("-"); // comment-789
    //         var comment_id = splitted[1];

    //         if ( settings.action == "dim-comment") {
    //             // approve / unapprove
    //             console.log("element! :: " + comment_id);
    //             console.log("approve / unapprove!");

    //             jQuery("#ebsih-hotspot-pin-" + comment_id).toggleClass('ebsih-hotspot-pending');
    //         }
    //         else if (settings.action == "delete-comment") {
    //             // trash / spam
    //             console.log("element! :: " + comment_id);
    //             console.log("trash!");
    //             jQuery("#ebsih-hotspot-pin-" + comment_id).hide();
    //         }
    //     }
    // });

    // pin select thickbox:
    $('a#ebish_comment_remove_icon').click(function(){
        console.log('removing icon');
        jQuery('#hotspot-icon-thickbox-a i').attr("class", '');
        $('#hotspotimage_settings\\[pin_icon\\]').val('');
        self.parent.tb_remove();
        return false;
    });

});