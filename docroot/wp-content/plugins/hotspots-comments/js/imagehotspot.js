function HotspotImage(hotspotImageID) {
	this.hotspotImageID 		= hotspotImageID;
	this.imageWrapperElement 	= jQuery("#ebsih-hotspot-wrapper-" + this.hotspotImageID);
	this.imageElement 			= this.imageWrapperElement.find('img');
	this.pinIcon 				= this.imageWrapperElement.data('pin-icon');
	this.pinColor 				= this.imageWrapperElement.data('pin-color');
	this.qtipStyle 				= this.imageWrapperElement.data('qtip-style');
	this.balloonPosition 		= this.imageWrapperElement.data('balloon-position');
	this.balloonShow 			= this.imageWrapperElement.data('balloon-show');
	this.ajaxifyForm 			= this.imageWrapperElement.data('ajaxify-form');

	this.hotspots = [];
	this.init();
	for (var i = 0; i < this.hotspots.length; i++) {
		this.attachBalloon(this.hotspots[i].pinElement.data('hotspot-id'));
	}
	if (this.ajaxifyForm) this.initForm();
}

HotspotImage.prototype.init = function() {
	hotspotID 	= this.hotspotImageID;
	hotspots 	= this.hotspots;

	switch(this.balloonPosition) {
		case 'bottom':
			this.qTipPosition = {my: 'top center', at: 'bottom center'};
			break;
		case 'left':
			this.qTipPosition = {my: 'center right', at: 'center left'};
			break;
		case 'right':
			this.qTipPosition = {my: 'center left', at: 'center right'};
			break;						
		default:
			this.qTipPosition = {my: 'bottom center', at: 'top center'}; // 'top'
			break;
	}

	if (this.balloonShow == 'click') {
		this.qTipShow = 'click';
	} else	{
		// 'hover'
		this.qTipShow = {
			solo: true,
			effect: function() {
				jQuery(this).fadeTo(200, 1);
			}
		};
	}

	// get hotspot objects
	jQuery("#ebsih-hotspot-wrapper-"+hotspotID+" .ebsih-hotspot-pin" ).each(function (index) {
		// create and store hotspot objects
		var hotspotObject = {
			pinElement: jQuery(this),
			balloonElement: jQuery("#ebsih-hotspot-balloon-"+ jQuery(this).data( "hotspot-id" )),
			x: jQuery(this).data( "x" ),
			y: jQuery(this).data( "y" ),
			id: jQuery(this).data( "hotspot-id" ),
		};
		hotspots.push(hotspotObject);
	});

	// bind cursor click
	jQuery("#ebsih-hotspot-wrapper-" + this.hotspotImageID + " img").click( function(e) {

		// hide all cursurs and comment boxes (if any)
		jQuery(".ebsih-hotspot-comment-box").hide();
		jQuery(".ensih-highlight-cursor").hide();

		// get current image and cursor
		hotspotImageID 	= jQuery(this).closest('.ebsih-hotspot-wrapper').data("hotspot-image-id"),
		cursorElement 	= jQuery("#ensih-highlight-cursor-"+hotspotImageID);

		commentsOpen = cursorElement.data("comments-open");
		if(commentsOpen) {
			// get relative mouse position on image
			var offset = jQuery(this).offset();
			var relativeX = (e.pageX - offset.left);
			var relativeY = (e.pageY - offset.top);

			// update highligh cursor
			cursorWidth 	= jQuery(cursorElement).width();
			cursorHeight 	= jQuery(cursorElement).height();
			cursorX = relativeX - (cursorWidth / 2);
			cursorY = relativeY - (cursorHeight / 2);
			cursorX = Math.max(0, cursorX); 
			cursorY = Math.max(0, cursorY); 
			cursorX = Math.min(cursorX, (jQuery(this).width() - cursorWidth)); 
			cursorY = Math.min(cursorY, (jQuery(this).height() - cursorHeight)); 
			jQuery(cursorElement).css({display: 'block', top: cursorY, left: cursorX});

			// update form field with position %
			percentageX = relativeX / jQuery(this).width();
			percentageY = relativeY / jQuery(this).height();
			jQuery('#ebsih-hotspot-values-' + hotspotImageID).val('{"x":'+percentageX+', "y":'+percentageY+'}');

			// show comment box for this image
			jQuery("#ebsih-hotspot-comment-box-" + hotspotImageID).show();

			jQuery("#ebsih-hotspot-comment-box-" + hotspotImageID + " form").show();
			jQuery("#ebsih-hotspot-comment-box-" + hotspotImageID + " .ebsih-notification").hide();

			if (jQuery("#ebsih-hotspot-comment-box-" + hotspotImageID + " input#author").length != 0) {
				jQuery("#ebsih-hotspot-comment-box-" + hotspotImageID + " input#author").focus();
			} else {
				jQuery("#ebsih-hotspot-comment-box-" + hotspotImageID + " textarea").focus();
			}
		}
	});

	// add icon to post-type comment link	
	var pinIcon = this.pinIcon;
		jQuery('.ebsih_comment_hotspot_link').each(function() {
		jQuery(this).addClass(pinIcon);
	});

	// show hotspot on link click
	jQuery('a.ebsih_comment_hotspot_link').click(function(e) {
		comment_id = jQuery(this).data('comment-id');
        jQuery('html, body').scrollTop ( jQuery("#ebsih-hotspot-pin-"+comment_id+"-1").offset().top - 500 );
		jQuery("#ebsih-hotspot-pin-"+comment_id+"-1").trigger('mouseenter');
		jQuery("#ebsih-hotspot-pin-"+comment_id+"-1").trigger('click');
		return false;
	});

}

HotspotImage.prototype.initForm = function() {
	var currrentHotspotImage = this;
    var commentBox = jQuery('#ebsih-hotspot-comment-box-'+ this.hotspotImageID);
    var commentform = jQuery('#ebsih-hotspot-comment-box-'+ this.hotspotImageID + ' form');

    commentform.prepend('<div class="comment-status" ></div>'); // add info panel before the form to provide feedback or errors
    var notificationDiv = this.imageWrapperElement.find('.ebsih-notification');
    
    commentform.submit(function() {

		//Display notification message	
   		notificationDiv.hide();
   		notificationDiv.attr('class', "ebsih-notification ebsih-notice");
   		notificationDiv.html('Sending message..');
   		notificationDiv.show();
   		notificationDiv.fadeIn();


        //serialize and store form data in a variable
        var formdata=commentform.serialize();

		commentBox.hide();
    
        //Extract action URL from commentform
        var formurl=commentform.attr('action');
        //Post Form with data
        jQuery.ajax({
            type: 'post',
            url: formurl,
            data: formdata,
            error: function(XMLHttpRequest, textStatus, errorThrown) {
            		// get error message from default WordPress error page:
					errorDocument = jQuery('<div/>');
					errorDocument.html(XMLHttpRequest.responseText);
				    errorText = ( errorDocument.find('p').first().html() );

               		notificationDiv.attr('class', "ebsih-notification ebsih-error");
               		notificationDiv.html(errorText);
               		notificationDiv.show();
               		notificationDiv.fadeIn();
	            	commentBox.show();

            },
            success: function(data, textStatus) {
                if (data=="success") {
               		notificationDiv.attr('class', "ebsih-notification ebsih-success");
               		notificationDiv.html('The message has been added.');
               		notificationDiv.show();
               		notificationDiv.fadeIn();
               		
               		tmpId = jQuery.now();
               		var positions = jQuery.parseJSON( jQuery('#ebsih-hotspot-values-' + hotspotImageID).val() );
               		currrentHotspotImage.createPin(tmpId, positions.x , positions.y);
               		currrentHotspotImage.createBalloon(tmpId, '(pending)', commentform.find('textarea[name=comment]').val());
               		currrentHotspotImage.attachBalloon(tmpId);

					jQuery(".ensih-highlight-cursor").hide();

					var hideFormTimeout = setTimeout(function() {
	               		commentform.find('textarea[name="comment"]').val('');
			            	commentform.find('input[type="submit"]').removeAttr('disabled'); //enable
						commentBox.hide();
						notificationDiv.hide();
					}, 2000 );
                }
                else {
               		notificationDiv.attr('class', "ebsih-notification ebsih-notification");
               		notificationDiv.html('Please wait a while before posting your next comment.');
               		notificationDiv.show();
                }
               
            },
            complete: function(data) {
            }
        });

        return false;
    });
}

HotspotImage.prototype.createPin = function(hotspotID, x, y) {
	pinElement = jQuery("<div id='ebsih-hotspot-pin-" + hotspotID + "' class='ebsih-hotspot-pin ebsih-hotspot-pending' style='color: "+ this.pinColor +";' ><i class='"+ this.pinIcon +"'></i></div>");
	jQuery('#ebsih-hotspot-image-wrapper-' + hotspotImageID).append(pinElement);

	// create and store new hotspot object
	var hotspotObject = {
		pinElement: pinElement,
		// balloonElement: jQuery("#ebsih-hotspot-balloon-"+ jQuery(this).data( "hotspot-id" )),
		x: x,
		y: y,
		id: hotspotID,
	};
	this.hotspots.push(hotspotObject);
	this.positionPins();
}

HotspotImage.prototype.createBalloon = function(hotspotID, author, content) {
	balloonElement = jQuery("<div class='ebsih-bubble' id='ebsih-hotspot-balloon-" + hotspotID + "' data-hotspot-id='" + hotspotID + "' data-hotspot-author='" + author + "' data-popup-style='" + this.qtipStyle + "'></div>");
	balloonElement.html(content);
	jQuery('#ebsih-hotspot-image-wrapper-' + hotspotImageID).append(balloonElement);
}

HotspotImage.prototype.attachBalloon = function(hotspotID) {

	pinElement = jQuery('#ebsih-hotspot-pin-' + hotspotID);
	balloonElement = jQuery('#ebsih-hotspot-balloon-' + hotspotID);

	balloonAuthor = balloonElement.data('hotspot-author');

	// add link if author url exists
	balloonAuthorUrl = balloonElement.data('hotspot-author-url');
	if(balloonAuthorUrl)
		balloonAuthor = '<a href="'+balloonAuthorUrl+'">'+balloonAuthor+'</a>';

	// determine pop-up position

	popupPosition = balloonElement.data('popup-position');
	if (popupPosition != 'default') {
		switch(popupPosition) {
			case 'bottom':
				this.qTipPosition = {my: 'top center', at: 'bottom center'};
				break;
			case 'left':
				this.qTipPosition = {my: 'center right', at: 'center left'};
				break;
			case 'right':
				this.qTipPosition = {my: 'center left', at: 'center right'};
				break;						
			default:
				this.qTipPosition = {my: 'bottom center', at: 'top center'}; // 'top'
				break;
		}
	}

	pinElement.qtip({ // apply tooltip
    	style: {
    		classes: "ebsih-qtip " + balloonElement.data('popup-style'),
    		width: "" + balloonElement.data('popup-width')
    	},
	    content: {
	        title: balloonAuthor,
	        text: balloonElement.html()
	    },
		position: {
			my: this.qTipPosition.my,
			at: this.qTipPosition.at,
		},
		show: this.qTipShow,
	    hide: {
	    	event: 'unfocus',
	    },
		events: {
			hide: function(event, api) {
				//reset iframes (stop videos)
				jQuery(".qtip-content iframe").each(function() { 
				        var src= jQuery(this).attr('src');
				        jQuery(this).attr('src',src);  
				});
			}
		},

	});
}

HotspotImage.prototype.position = function() {

	// check if image has dimensions
	if(this.imageElement.width() > 0) {

		this.positionPins();

		//resize form width
		jQuery("#ebsih-hotspot-comment-box-" + this.hotspotImageID).width(this.imageElement.width());

		jQuery(window).scrollTop()
	}
	else {
		// recursive check (in case of obscured by tabs / carrousels etc)
		// this.position_timeout = setTimeout(function() {
	 	//  this.position();
		// }.bind(this), 1000);
	}
}

HotspotImage.prototype.positionPins = function() {

	for (var i = 0; i < this.hotspots.length; i++) {
		var hotspot = this.hotspots[i];
		var position = hotspot.pinElement.position();

		pixelsLeft 	= parseInt(hotspot.x * this.imageElement.width());
		pixelsTop 	= parseInt(hotspot.y * this.imageElement.height());

		pixelsLeft 	-= (hotspot.pinElement.outerWidth() / 2);
		pixelsTop 	-= (hotspot.pinElement.outerHeight() /2);

		hotspot.pinElement.css({top: pixelsTop + "px", left: pixelsLeft + "px", opacity: 1});
	}

};

jQuery(document).ready(function($){

	// qTip2 Chrome position bugfix
	var isOpera = !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
	var isChrome = !!window.chrome && !isOpera;// Chrome 1+
	if(isChrome) {
		if(
			( $("body").css('overflow-x') != 'visible' || $("body").css('overflow-y') != 'visible' ) &&
			$("body").css('position') != 'static'

		) {
			if ($("body").css('overflow-x') != 'visible') { $("body").css('overflow-x', 'initial'); };
			if ($("body").css('overflow-y') != 'visible') { $("body").css('overflow-y', 'initial'); };
		}
	}

	// create  HotspotImage objects
	var ebishHotspotImages = [];
	jQuery( ".ebsih-hotspot-wrapper" ).each(function( index ) {
		var myHotspotImage = new HotspotImage( jQuery(this).data( "hotspot-image-id" ) );
		ebishHotspotImages.push(myHotspotImage);
	});

	// hide stuff when clicking outside image or hotspots
	$(document).click(function(event) { 
		if(
			!$(event.target).closest('.ebsih-hotspot-image-wrapper img').length &&
			!$(event.target).closest('.ebsih-hotspot-comment-box').length
		) {
			$(".ebsih-hotspot-comment-box").hide();
			$(".ensih-highlight-cursor").hide();
			$('.ebsih-notification').hide();
		}

		if ($(event.target).closest('.ensih-highlight-cursor').length) {
			$(".ebsih-hotspot-comment-box").hide();
			$(".ensih-highlight-cursor").hide();
			$('.ebsih-notification').hide();
		}
	})

	// close form on cross-icon click
	$('a.close-hotspot-form').click(function() {
		// hide all cursurs and comment boxes (if any)
		$(".ebsih-hotspot-comment-box").hide();
		$(".ensih-highlight-cursor").hide();
		$('.ebsih-notification').hide();
		return false;
	});

	// update positions after resize
	var resizeId;
	$( window ).resize(function() {
		for (var i = 0; i < ebishHotspotImages.length; i++) {
			var hotspotImage = ebishHotspotImages[i];
			hotspotImage.position();
		}
	});
		
	// after load position re-position (webkit)
	window.addEventListener('load', function () {
		for (var i = 0; i < ebishHotspotImages.length; i++) {
			var hotspotImage = ebishHotspotImages[i];
			hotspotImage.position();
		}
	}, false);

	if ("onhashchange" in window) // does the browser support the hashchange event?
	{
    	window.onhashchange = function () {
        	jQuery(document).scrollTop();  
        	return false;
    	}
	}
});