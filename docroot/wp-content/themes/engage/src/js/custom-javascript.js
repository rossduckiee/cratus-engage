jQuery(window).on('load', function(){
    jQuery('#cover').fadeOut(400);
    this.$slideOut = jQuery('#slideOut');

    // Slideout show
    this.$slideOut.find('.slideOutTab').on('click', function() {
      jQuery("#slideOut").toggleClass('showSlideOut');
	  jQuery(".imp-fullscreen-tooltips-container").addClass('d-none');

    });

    jQuery( ".imp-shape" ).click(function() {
	  if (!jQuery("#slideOut").hasClass("showSlideOut")) {
	  	jQuery(".imp-fullscreen-tooltips-container").removeClass('d-none');
	  }
	});

})

jQuery(function() {
  jQuery(window).load(function() {
  	if (jQuery(window).width() > 768) {
  		if (jQuery(".full-width-section").hasClass("h-scrollable")) {
		    var $gal = jQuery(".h-scrollable"),
		      galW = $gal.outerWidth(true),
		      galSW = $gal[0].scrollWidth,
		      wDiff = (galSW / galW) - 1, // widths difference ratio
		      mPadd = 30, // mousemove Padding
		      damp = 30, // Mmusemove response softness
		      mX = 0, // real mouse position
		      mX2 = 0, // modified mouse position
		      posX = 0,
		      mmAA = galW - (mPadd * 2), // the mousemove available area
		      mmAAr = (galW / mmAA); // get available mousemove didderence ratio
		    $gal.mousemove(function(e) {
		      mX = e.pageX - jQuery(this).parent().offset().left - this.offsetLeft;
		      mX2 = Math.min(Math.max(0, mX - mPadd), mmAA) * mmAAr;
		    });
		    setInterval(function() {
		      posX += (mX2 - posX) / damp; // zeno's paradox equation "catching delay"	
		      $gal.scrollLeft(posX * wDiff);
		    }, 10);
		}
	}
  });
});

jQuery(document).on('gform_post_render', function(){
  jQuery(".emoji-support .slider").change(function(){
  	var $value = jQuery(".emoji-support .slider").val();
  	jQuery('.emoji-support .noUi-tooltip').attr('class', function(i, c){
	    return c.replace(/(^|\s)value-\S+/g, '');
	});
	jQuery('.emoji-support .noUi-tooltip').addClass('value-' + $value);
  });
});

jQuery(function() {
	var redirectCookie = Cookies.get('returning-visitor');
	if (redirectCookie == null){
		Cookies.set('returning-visitor', 'value', { expires: 365 })
		window.location = "/welcome"
	}
	var open = false;

	jQuery('#navbar-toggle').click(function() {
	  if(!open) {
	    showMenu();
	    jQuery('#navbar-toggle label').hide();
	    jQuery('#navbar-toggle .close-icon').show();
	    
	  }
	  else {
	    hideMenu();
	    jQuery('#navbar-toggle .close-icon').hide();
	    jQuery('#navbar-toggle label').show();
	  }
	});

	

	function hideMenu() {
	  jQuery('nav').removeClass('menu-left-open');
	  jQuery('.site').removeClass('menu-push-right');
	  open = false;
	};

	function showMenu() {
	  jQuery('nav').addClass('menu-left-open');
	  jQuery('.site').addClass('menu-push-right');
	  open = true;
	};
});

jQuery(window).on('load',function(){
    jQuery('#foo').modal('show');
});

jQuery(document).ready(function() {
    setInterval(function() {
        var docHeight = jQuery(window).height();
        var footerHeight = jQuery('#footer').height();
        var footerTop = jQuery('#footer').position().top + footerHeight;
        var marginTop = (docHeight - footerTop + 10);

        if (footerTop < docHeight)
            jQuery('#footer').css('margin-top', marginTop + 'px'); // padding of 30 on footer
        else
            jQuery('#footer').css('margin-top', '0px');
        // console.log("docheight: " + docHeight + "\n" + "footerheight: " + footerHeight + "\n" + "footertop: " + footerTop + "\n" + "new docheight: " + $(window).height() + "\n" + "margintop: " + marginTop);
    }, 250);
});

if( document.body.className.match('page-template-confetti-page') ) {
/**
 * Confetti particle class
 */
class ConfettiParticle {
  
  constructor( context, width, height ) {
    this.context = context;
    this.width = width;
    this.height = height;
    this.color = '';
    this.lightness = 60; 
    this.diameter = 0;
    this.tilt = 0;
    this.tiltAngleIncrement = 0;
    this.tiltAngle = 0;
    this.particleSpeed = 1;
    this.waveAngle = 0;
    this.x = 0;
    this.y = 0;
    this.reset();
  }
  
  reset() {
    this.lightness = 50;
    this.color = Math.floor( Math.random() * 360 ); 
    this.x = Math.random() * this.width;
    this.y = Math.random() * this.height - this.height;
    this.diameter = Math.random() * 6 + 4;
    this.tilt = 0;
    this.tiltAngleIncrement = Math.random() * 0.1 + 0.04;
    this.tiltAngle = 0;
  }
  
  darken() {
    if ( this.y < 100 || this.lightness <= 0 ) return; 
    this.lightness -= ( 200 / this.height ); 
  }
  
  update() {
    this.waveAngle += this.tiltAngleIncrement;
    this.tiltAngle += this.tiltAngleIncrement;
    this.tilt = Math.sin( this.tiltAngle ) * 12;
    this.x += Math.sin( this.waveAngle );
    this.y += ( Math.cos( this.waveAngle ) + this.diameter + this.particleSpeed ) * 0.4;
    if ( this.complete() ) this.reset(); 
    this.darken();
  }
  
  complete() {
    return ( this.y > this.height + 20 );
  }
  
  draw() {
    let x = this.x + this.tilt;
    this.context.beginPath();
    this.context.lineWidth = this.diameter;
    this.context.strokeStyle = "hsl("+ this.color +", 50%, "+this.lightness+"%)";
    this.context.moveTo( x + this.diameter / 2, this.y );
    this.context.lineTo( x, this.y + this.tilt + this.diameter / 2 );
    this.context.stroke();
  }
}

/**
 * Setup
 */
(function() {
  let width = window.innerWidth;
  let height = window.innerHeight;
  let particles = [];

  // particle canvas
  const canvas = document.createElement( 'canvas' );
  const context = canvas.getContext( '2d' );
  canvas.id = 'particle-canvas';
  canvas.width = width;
  canvas.height = height;
  document.body.appendChild( canvas );

  // update canvas size
  const updateSize = () => {
    width = window.innerWidth;
    height = window.innerHeight;
    canvas.width = width;
    canvas.height = height;
  };
  
  // create confetti particles 
  const createParticles = () => {
    particles = []; 
    let total = 100; 
    
    if ( width > 1080 ) { total = 400; } else 
    if ( width > 760 )  { total = 300; } else 
    if ( width > 520 )  { total = 200; }
    
    for ( let i = 0; i < total; ++i ) {
      particles.push( new ConfettiParticle( context, width, height ) );
    }
  };

  // animation loop function
  const animationFunc = () => {
    requestAnimationFrame( animationFunc );
    context.clearRect( 0, 0, width, height );
    
    for ( let p of particles ) {
      p.width = width;
      p.height = height;
      p.update();
      p.draw();
    }
  };
  
  // on resize 
  window.addEventListener( 'resize', e => {
    updateSize();
    createParticles();
  });

  // start
  updateSize();
  createParticles();
  animationFunc();
  
})();
}