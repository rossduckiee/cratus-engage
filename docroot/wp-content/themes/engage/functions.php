<?php
/**
 * engage functions and definitions
 *
 * @package engage
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

$engage_includes = array(
	'/theme-settings.php',                  // Initialize theme default settings.
	'/setup.php',                           // Theme setup and custom theme supports.
	'/widgets.php',                         // Register widget area.
	'/enqueue.php',                         // Enqueue scripts and styles.
	'/template-tags.php',                   // Custom template tags for this theme.
	'/pagination.php',                      // Custom pagination for this theme.
	'/hooks.php',                           // Custom hooks.
	'/extras.php',                          // Custom functions that act independently of the theme templates.
	'/customizer.php',                      // Customizer additions.
	'/custom-comments.php',                 // Custom Comments file.
	'/jetpack.php',                         // Load Jetpack compatibility file.
	'/class-wp-bootstrap-navwalker.php',    // Load custom WordPress nav walker. Trying to get deeper navigation? Check out: https://github.com/engage/engage/issues/567.
	'/woocommerce.php',                     // Load WooCommerce functions.
	'/editor.php',                          // Load Editor functions.
	'/deprecated.php',                      // Load deprecated functions.
);

foreach ( $engage_includes as $file ) {
	require_once get_template_directory() . '/inc' . $file;
}

// disable for posts
add_filter('use_block_editor_for_post', '__return_false', 10);

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Header Settings',
		'menu_title'	=> 'Header',
		'parent_slug'	=> 'theme-general-settings',
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Footer Settings',
		'menu_title'	=> 'Footer',
		'parent_slug'	=> 'theme-general-settings',
	));
	
}

/*
* Creating a function to create our CPT
*/
 
function document_library() {
 
// Set UI labels for Custom Post Type
    $labels = array(
        'name'                => _x( 'documents', 'Post Type General Name', 'engage' ),
        'singular_name'       => _x( 'Document', 'Post Type Singular Name', 'engage' ),
        'menu_name'           => __( 'Documents', 'engage' ),
        'parent_item_colon'   => __( 'Parent Document', 'engage' ),
        'all_items'           => __( 'All documents', 'engage' ),
        'view_item'           => __( 'View Document', 'engage' ),
        'add_new_item'        => __( 'Add New Document', 'engage' ),
        'add_new'             => __( 'Add New', 'engage' ),
        'edit_item'           => __( 'Edit Document', 'engage' ),
        'update_item'         => __( 'Update Document', 'engage' ),
        'search_items'        => __( 'Search Document', 'engage' ),
        'not_found'           => __( 'Not Found', 'engage' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'engage' ),
    );
     
// Set other options for Custom Post Type
     
    $args = array(
        'label'               => __( 'documents', 'engage' ),
        'description'         => __( 'Consultation Document', 'engage' ),
        'labels'              => $labels,
        // Features this CPT supports in Post Editor
        'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
        // You can associate this CPT with a taxonomy or custom taxonomy.
        /* A hierarchical CPT is like Pages and can have
        * Parent and child items. A non-hierarchical CPT
        * is like Posts.
        */ 
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'menu_icon'           => 'dashicons-welcome-add-page',
        'can_export'          => true,
        'has_archive'         => false,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',
        'show_in_rest' => true,
 
    );
    
    if ( ! get_field( 'libraries','options' ) && ! get_field('document_library','options') ){
    // Registering your Custom Post Type
    register_post_type( 'documents', $args );
    }
 
}

/* Hook into the 'init' action so that the function
* Containing our post type registration is not 
* unnecessarily executed. 
*/
 
add_action( 'init', 'document_library', 0 );

function video_library() {
 
// Set UI labels for Custom Post Type
    $labels = array(
        'name'                => _x( 'Videos', 'Post Type General Name', 'engage' ),
        'singular_name'       => _x( 'Video', 'Post Type Singular Name', 'engage' ),
        'menu_name'           => __( 'Videos', 'engage' ),
        'parent_item_colon'   => __( 'Parent Video', 'engage' ),
        'all_items'           => __( 'All Videos', 'engage' ),
        'view_item'           => __( 'View Video', 'engage' ),
        'add_new_item'        => __( 'Add New Video', 'engage' ),
        'add_new'             => __( 'Add New', 'engage' ),
        'edit_item'           => __( 'Edit Video', 'engage' ),
        'update_item'         => __( 'Update Video', 'engage' ),
        'search_items'        => __( 'Search Video', 'engage' ),
        'not_found'           => __( 'Not Found', 'engage' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'engage' ),
    );
     
// Set other options for Custom Post Type
     
    $args = array(
        'label'               => __( 'Videos', 'engage' ),
        'description'         => __( 'Consultation Video', 'engage' ),
        'labels'              => $labels,
        // Features this CPT supports in Post Editor
        'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
        // You can associate this CPT with a taxonomy or custom taxonomy.
        /* A hierarchical CPT is like Pages and can have
        * Parent and child items. A non-hierarchical CPT
        * is like Posts.
        */ 
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'menu_icon'           => 'dashicons-video-alt2',
        'can_export'          => true,
        'has_archive'         => false,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',
        'show_in_rest' => true,
 
    );
    if ( ! get_field( 'libraries','options' ) && ! get_field('webinar_library','options') ){
        // Registering your Custom Post Type
        register_post_type( 'videos', $args );
    }
 
}

add_action( 'init', 'video_library', 0 );

function team() {
 
// Set UI labels for Custom Post Type
    $labels = array(
        'name'                => _x( 'Team Member', 'Post Type General Name', 'engage' ),
        'singular_name'       => _x( 'Team Member', 'Post Type Singular Name', 'engage' ),
        'menu_name'           => __( 'Team Members', 'engage' ),
        'parent_item_colon'   => __( 'Parent Team Member', 'engage' ),
        'all_items'           => __( 'All Team Members', 'engage' ),
        'view_item'           => __( 'View Team Member', 'engage' ),
        'add_new_item'        => __( 'Add New Team Member', 'engage' ),
        'add_new'             => __( 'Add New', 'engage' ),
        'edit_item'           => __( 'Edit Team Member', 'engage' ),
        'update_item'         => __( 'Update Team Member', 'engage' ),
        'search_items'        => __( 'Search Team Member', 'engage' ),
        'not_found'           => __( 'Not Found', 'engage' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'engage' ),
    );
     
// Set other options for Custom Post Type
     
    $args = array(
        'label'               => __( 'Team Members', 'engage' ),
        'description'         => __( 'Consultation Team Member', 'engage' ),
        'labels'              => $labels,
        // Features this CPT supports in Post Editor
        'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
        // You can associate this CPT with a taxonomy or custom taxonomy.
        /* A hierarchical CPT is like Pages and can have
        * Parent and child items. A non-hierarchical CPT
        * is like Posts.
        */ 
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'menu_icon'           => 'dashicons-admin-users',
        'can_export'          => true,
        'has_archive'         => false,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',
        'show_in_rest' => true,
 
    );
     
    // Registering your Custom Post Type
    register_post_type( 'team', $args );
 
}

add_action( 'init', 'team', 0 );


 
function responses() {
 
// Set UI labels for Custom Post Type
    $labels = array(
        'name'                => _x( 'responses', 'Post Type General Name', 'engage' ),
        'singular_name'       => _x( 'response', 'Post Type Singular Name', 'engage' ),
        'menu_name'           => __( 'Responses', 'engage' ),
        'parent_item_colon'   => __( 'Parent response', 'engage' ),
        'all_items'           => __( 'All responses', 'engage' ),
        'view_item'           => __( 'View response', 'engage' ),
        'add_new_item'        => __( 'Add New response', 'engage' ),
        'add_new'             => __( 'Add New', 'engage' ),
        'edit_item'           => __( 'Edit response', 'engage' ),
        'update_item'         => __( 'Update response', 'engage' ),
        'search_items'        => __( 'Search response', 'engage' ),
        'not_found'           => __( 'Not Found', 'engage' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'engage' ),
    );
     
// Set other options for Custom Post Type
     
    $args = array(
        'label'               => __( 'responses', 'engage' ),
        'description'         => __( 'Consultation response', 'engage' ),
        'labels'              => $labels,
        // Features this CPT supports in Post Editor
        'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
        // You can associate this CPT with a taxonomy or custom taxonomy.
        /* A hierarchical CPT is like Pages and can have
        * Parent and child items. A non-hierarchical CPT
        * is like Posts.
        */ 
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'menu_icon'           => 'dashicons-format-chat',
        'can_export'          => true,
        'has_archive'         => false,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',
        'show_in_rest' => true,
 
    );
    
    if ( ! get_field( 'ideas_wall','options' ) ){
        // Registering your Custom Post Type
        register_post_type( 'responses', $args );
    }
 
}

/* Hook into the 'init' action so that the function
* Containing our post type registration is not 
* unnecessarily executed. 
*/
 
add_action( 'init', 'responses', 0 );


function polls() {
 
// Set UI labels for Custom Post Type
    $labels = array(
        'name'                => _x( 'polls', 'Post Type General Name', 'engage' ),
        'singular_name'       => _x( 'poll', 'Post Type Singular Name', 'engage' ),
        'menu_name'           => __( 'Polls', 'engage' ),
        'parent_item_colon'   => __( 'Parent poll', 'engage' ),
        'all_items'           => __( 'All polls', 'engage' ),
        'view_item'           => __( 'View poll', 'engage' ),
        'add_new_item'        => __( 'Add New poll', 'engage' ),
        'add_new'             => __( 'Add New', 'engage' ),
        'edit_item'           => __( 'Edit poll', 'engage' ),
        'update_item'         => __( 'Update poll', 'engage' ),
        'search_items'        => __( 'Search poll', 'engage' ),
        'not_found'           => __( 'Not Found', 'engage' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'engage' ),
    );
     
// Set other options for Custom Post Type
     
    $args = array(
        'label'               => __( 'polls', 'engage' ),
        'description'         => __( 'Consultation poll', 'engage' ),
        'labels'              => $labels,
        // Features this CPT supports in Post Editor
        'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
        // You can associate this CPT with a taxonomy or custom taxonomy.
        /* A hierarchical CPT is like Pages and can have
        * Parent and child items. A non-hierarchical CPT
        * is like Posts.
        */ 
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'menu_icon'           => 'dashicons-editor-help',
        'can_export'          => true,
        'has_archive'         => false,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',
        'show_in_rest' => true,
 
    );
     
    // Registering your Custom Post Type
    register_post_type( 'polls', $args );
 
}

/* Hook into the 'init' action so that the function
* Containing our post type registration is not 
* unnecessarily executed. 
*/
 
add_action( 'init', 'polls', 0 );

function engage_button_shortcode( $atts ){
    return '<div class="button-cta-shortcode d-flex mt-3">
                    <a class="btn btn-primary mr-4" href="/polls">Back to polls <i class="fa fa-angle-right mg-l-5"></i></a>
                    <a class="btn btn-primary" href="/">Back to main site <i class="fa fa-angle-right mg-l-5"></i></a>
                </div>';
}
add_shortcode( 'buttons', 'engage_button_shortcode' );

function engage_home_button_shortcode( $atts ){
    return '<div class="button-cta-shortcode d-flex mt-3">
                    <a class="btn btn-primary" href="/">Back to main site <i class="fa fa-angle-right mg-l-5"></i></a>
                </div>';
}
add_shortcode( 'homebutton', 'engage_home_button_shortcode' );

add_action('admin_bar_menu', 'add_toolbar_items', 100);
function add_toolbar_items($admin_bar){
    $admin_bar->add_menu( array(
        'id'    => 'flush--all--caches',
        'title' => 'FLUSH ALL CACHES',
        'href'  => 'https://www.cratusengage.co.uk/wp-admin/admin.php?page=litespeed-toolbox&LSCWP_CTRL=PURGE_EMPTYCACHE&LSCWP_NONCE=df71bae1cf',
        'meta'  => array(
            'title' => __('Flush all Caches'),            
        ),
    ));
}

add_action('admin_head', 'my_custom_fonts');

function my_custom_fonts() {
  echo '<style>
    #wp-admin-bar-flush--all--caches {
        background-color: #bb1515 !important;
    }
  </style>';
}

