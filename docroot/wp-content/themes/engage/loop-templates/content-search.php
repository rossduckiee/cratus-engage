<?php
/**
 * Search results partial template
 *
 * @package engage
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;
?>

<div class="list-group-item d-block pd-y-20 mb-4">
  <div class="d-flex justify-content-between align-items-center tx-12 mg-b-10">
    <a href="" class="tx-info">Search Result</a>
    <span><?php the_date();?></span>
  </div><!-- d-flex -->
  <h6 class="lh-3 mg-b-10"><a href="<?php the_permalink();?>" class="tx-inverse hover-primary"><?php the_title();?></a></h6>
  <p class="tx-13 mg-b-0 tx-inverse"><?php $content = get_the_content(); echo mb_strimwidth($content, 0, 400, '...');?></p>
  <a href="<?php the_permalink();?>" class="btn btn-primary mt-3 mg-b-10">Read More</a>
</div>

