<?php
/**
 * Post rendering content according to caller of get_template_part
 *
 * @package engage
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;
?>



<div class="col-md-4">

 	<a class="section-teasers" href="<?php the_permalink();?>">
     	<div class="image-container home-teasers">
     		<?php echo get_the_post_thumbnail( $post->ID, 'large' ); ?>
     	</div>
     	<div class="lower-teaser-content">
     		<h4><?php the_title();?></h4>
			<div class="date"><?php the_date();?></div>
     		<div class="icon-click">
     			<img src="<?php echo get_template_directory_uri(); ?>/assets/chevron.svg">
     		</div>
     	</div>
     </a>

 </div>


