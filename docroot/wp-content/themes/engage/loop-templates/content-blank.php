<?php
/**
 * Blank content partial template
 *
 * @package engage
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

the_content();
