<?php
/**
 * Single post partial template
 *
 * @package engage
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;
?>

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

	<header class="entry-header">

		<h1 class="slim-pagetitle mb-4"><?php the_title();?></h1>

		

	</header><!-- .entry-header -->

	<div class="entry-content">

		<?php if(is_singular( 'responses' )) :?>

				<div class="card">
	                <div class="card-body">
	                  <div class="row mt-4">
						<?php if ( has_post_thumbnail() ) :?>
							<div class="col-md-6">
	    						<?php echo the_post_thumbnail('large', array('class' => 'img-thumbnail')); ?>
	    					</div>
	    				<?php endif;?>
				    	<div class="col-md-<?php if ( has_post_thumbnail() ) :?>6<?php else:?>12<?php endif;?>">
				    		<?php the_content();?>
				    		<a href="/ideas-wall/" class="btn btn-outline-primary <?php if ( has_post_thumbnail() ) :?>w-100<?php endif;?>">Back to idea wall</a>
				    	</div>
					</div>
	                </div><!-- card-body -->
	                <div class="card-footer bd-t">
	                  <?php the_date();?>
	                </div><!-- card-footer -->
              	</div>
        <?php else:?>
        	<div class="single-date tx-inverse mb-4"><?php the_date();?></div>
        	<div class="tx-inverse">
        		<?php the_content();?>
        		<?php if(is_singular( 'post' )) :?>
        			<a href="/news-updates/" class="btn btn-outline-primary">Back to News & Updates</a>
        		<?php endif;?>
        	</div>
		<?php endif;?>



		<?php if( get_field('youtube') ): ?>

			<div class="video-content">
				<h3 class="mb-4 mt-4 tx-inverse">Watch now</h3>
				<div class="videoWrapper mb-3">
					<?php the_field('youtube');?>
				</div>
				<a href="/video-library/" class="btn btn-outline-primary w-100 mb-4">Back to Video Library</a>
			</div>
			
		<?php endif;?>

	</div><!-- .entry-content -->

</article><!-- #post-## -->
