<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package engage
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

$container = get_theme_mod( 'engage_container_type' );
?>

<?php get_template_part( 'sidebar-templates/sidebar', 'footerfull' ); ?>

<div id="footer">
 <div class="slim-footer mt-0">
  <div class="container">
	  <div class="col">
    	<p>Copyright <?php echo date( 'Y' ); ?> &copy; All Rights Reserved.<br />Engage is a product of <a rel="noopener" href="https://cratus.co.uk" target="_blank">Cratus Communications Ltd.</a></p>
	  </div>
  </div><!-- container -->
</div><!-- slim-footer -->
</div>

</div><!-- #page we need this extra closing tag here -->
<?php wp_footer(); ?>
</body>
</html>