<?php
/**
 * The template for displaying search forms
 *
 * @package engage
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;
?>

<form method="get" id="searchform" class="search-box" action="<?php echo esc_url( home_url( '/' ) ); ?>" role="search">
	
		<input class="field form-control" id="s" name="s" type="text" placeholder="Search" aria-label="Search" value="<?php the_search_query(); ?>">
			<button class="submit btn btn-primary" id="searchsubmit" name="submit" type="submit"><i class="fa fa-search"></i><span class="sr-only">Search</span></button>
	
</form>


