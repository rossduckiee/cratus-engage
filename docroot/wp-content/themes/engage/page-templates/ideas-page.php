<?php
/**
 * Template Name: Responses Template
 *
 * This template can be used to override the default template and sidebar setup
 *
 * @package duckiee
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();
?>

<div class="wrapper" id="page-wrapper">

	<div class="container" id="content">

		<h1 class="slim-pagetitle mb-4"><?php the_title();?></h1>

		<?php if ( ! get_field( 'ideas_wall','options' ) ):?>

			<?php the_content();?>

			<div class='row'>
				<div class="col-lg-3">
					<div class="card tx-center mb-4">
	            		<div class="card-body pd-40">
	              			<div class="d-flex justify-content-center mg-b-30">
	                			<img alt="icon of a lightbulb" src="<?php echo get_template_directory_uri(); ?>/assets/idea.svg">
	              			</div>
	              			<h6 class="tx-md-20 tx-inverse mg-b-20">Your Ideas</h6>
	              			<p>Welcome to the ideas wall, please leave a sticky note here with your idea for public realm improvements, you can also upload images.</p> <p>All entries are moderated (for inappropriate or abusive language) before going live. If you would like to show your support for someone else's idea please click on the thumbs up, if you would like to indicate your disagreement with someone else's idea please click on the thumbs down.</p>
	              			<a href="" class="btn btn-primary btn-block" data-toggle="modal" data-target="#create-modal">Create</a>
	            		</div><!-- card -->
	          		</div>
				</div>
				<div class="col-lg-9">
					
						<?php $eventargs = array(
							    'posts_per_page' => 9,
							    'facetwp' => true, 
							    'post_type' => 'responses',
								'order'		=> 'ASC',
							);

							$eventquery = new WP_Query( $eventargs );
									 
								if ( $eventquery->have_posts() ) {

									echo '<div class="row">';
								 
								    while ( $eventquery->have_posts() ) {
								 
								        $eventquery->the_post();?>
								       	<div class="col-xl-4 col-lg-6 col-md-6 col-12">
								       		<div class="postit">
								       			<div class="content">
								       				<div class="mb-3">
									       				<?php $content = get_the_content(); echo mb_strimwidth($content, 0, 140, '...');?>
									       			</div>
									       			<a href="<?php the_permalink();?>">Read More</a>
									       			<div class="rating">
													  <?php echo do_shortcode('[posts_like_dislike]');?>
													</div>
									       		</div>
								       		</div>
										</div>
								    <?php } ?>

									</div>

									<div class="mt-3">

								    <?php engage_pagination(['total' => $eventquery->max_num_pages,]); ?>

									</div>

						        <?php } else { ?>
						        <div class="col-xl-4 col-lg-6 col-md-6 col-12">
						       		<div class="postit">
						       			<div class="content">
						       				<div class="mb-3">
							       				We've not had any ideas or responses submitted yet, why not be the first?
							       			</div>
							       			
							       		</div>
						       		</div>
								</div>
								
							<?php } wp_reset_postdata(); ?>
						</div>
					</div>
				</div>
			</div>
			<div id="create-modal" class="modal fade" style="display: none;" aria-hidden="true">
		      <div class="modal-dialog modal-dialog-vertical-center" role="document">
		        <div class="modal-content bd-0 tx-14">
		          <div class="modal-header">
		            <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Create Sticky Note</h6>
		            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		              <span aria-hidden="true">×</span>
		            </button>
		          </div>
		          <div class="modal-body pd-25">
		           <?php echo do_shortcode('[gravityform id="3" title="false" description="false" ajax="true" tabindex="49"]');?>
		          </div>
		        </div>
		      </div><!-- modal-dialog -->
		    </div>
		    <?php else:?>
		    	<p>This feature is currently not active, please contact your site administrator to enquire about adding it.</p>
		    <?php endif;?>

	</div><!-- #content -->

</div><!-- #page-wrapper -->

<?php
get_footer();

