<?php
/**
 * Template Name: Welcome Page
 *
 * Template for displaying the welcome page.
 *
 * @package engage
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();
$container = get_theme_mod( 'engage_container_type' );
?>

<div class="wrapper p-0" id="full-width-page-wrapper">
	<div class="container welcome-container">
		<div class="content-box mt-5 mt-md-0">
			<h1>Welcome and thank you for visiting</h1>
			<h4>Cratus Engage</h4>
			<?php the_content();?>
			<div class="row mt-5">
				<div class="feature-icon col-lg-3 col-md-6 col-6">
					<img alt="Icon of a computer" src="<?php echo get_template_directory_uri(); ?>/assets/virtual.svg">
					<h4 class="mt-3">Virtual Consultation</h4>
				</div>
				<div class="feature-icon col-lg-3 col-md-6 col-6">
					<img alt="Icon of a timeliner" src="<?php echo get_template_directory_uri(); ?>/assets/process.svg">
					<h4 class="mt-3">Process & Timeline</h4>
				</div>
				<div class="feature-icon col-lg-3 col-md-6 col-6">
					<img alt="Icon of an alert" src="<?php echo get_template_directory_uri(); ?>/assets/alert.svg">
					<h4 class="mt-3">Development Updates</h4>
				</div>
				<div class="feature-icon col-lg-3 col-md-6 col-6">
					<img alt="Icon of a speech bubble" src="<?php echo get_template_directory_uri(); ?>/assets/feedback.svg">
					<h4 class="mt-3">Give your Feedback</h4>
				</div>
			</div>
			<div class="lower-content">
				<a href="#" data-toggle="modal" data-target="#largeModal" class="btn btn-default" title="register to continue">Register to continue</a>
				<a href="/" title="skip registration" class="skip-link-lower">I've already registered</i></a>
			</div>
		</div>
	</div>
</div><!-- #full-width-page-wrapper -->

<div class="modal fade" id="largeModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Rockwell Property - Kensington Forum Hotel</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <?php echo do_shortcode('[gravityform id="2" title="false" description="false" ajax="true" tabindex="49"]');?>
      </div>
    </div>
  </div>
</div>

<?php wp_footer(); ?>
