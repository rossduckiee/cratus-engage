<?php
/**
 * Template Name: Document Library Page
 *
 * Template for displaying a documents
 * @package engage
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();
$container = get_theme_mod( 'engage_container_type' );
?>

<div class="wrapper" id="full-width-page-wrapper">
	<div class="container">
		<?php the_content();?>
		<div class="row">
			<div class="col-md-12 mb-3">
				<h1 class="slim-pagetitle mb-4"><?php the_title();?></h1>
			</div>
			<div class="col-md-4 side-nav">
				<h5 class="tx-inverse mb-3">Quick Links</h5>
				<div class="quick-filters nav">
				<?php if (   ! get_field('document_library','options') ) :?>
		              <a href="/document-library" class="nav-link active">
		                <span>Documents</span>
		              </a>
		        <?php endif;?>
				<?php if (   ! get_field('webinar_library','options') ) :?>
					<a href="/video-library" class="nav-link">
	                	<span>Webinars / Videos</span>
	              	</a>
              	<?php endif;?>
              <a href="/polls" class="nav-link">
                <span>Feedback</span>
              </a>
              <?php if ( ! get_field( 'timeline','options' ) ): ?>
	              <a href="/process" class="nav-link">
	                <span>Timeline</span>
	              </a>
	          <?php endif;?>
	          <?php if ( ! get_field( 'latest_news','options' ) ): ?>
	              <a href="/news-updates" class="nav-link">
	                <span>Latest News & Updates</span>
	              </a>
	          <?php endif;?>
            </div>
			
			</div>
			<div class="col-md-8">
				<h5 class="tx-inverse mb-3">Latest Documents</h5>
			<?php $eventargs = array(
				    'posts_per_page' => -1,
				    'post_type' => 'documents',
					'order'		=> 'ASC',
				);

				$eventquery = new WP_Query( $eventargs );
						 
					if ( $eventquery->have_posts() ) {
					 
					    while ( $eventquery->have_posts() ) {
					 
					        $eventquery->the_post();?>
					        <div class="file-item mb-4">
				                <div class="row no-gutters wd-100p">
				                  <div class="col-9 col-sm-5 d-flex align-items-center">
				                    <i class="fa fa-file-image-o mr-2"></i>
				                    <?php the_title();?>
				                  </div><!-- col-6 -->
				                  <div class="col-3 col-sm-2 tx-right tx-sm-left">PDF</div>
				                  <div class="col-6 col-sm-4 mg-t-5 mg-sm-t-0"><?php echo get_the_date(); ?></div>
				                  <div class="col-6 col-sm-1 tx-right mg-t-5 mg-sm-t-0"><a target="_blank" class="download-icon" href="<?php the_field('file');?>"><img alt="download icon (Click to download)" src="<?php echo get_template_directory_uri(); ?>/assets/download.svg"></a></div>
				                </div><!-- row -->
				              </div>
					    <?php } ?>

			        <?php } else { ?>
					
				<?php } wp_reset_postdata(); ?>
			</div>
		</div>
	</div>
</div><!-- #full-width-page-wrapper -->

<?php
get_footer();
