<?php
/**
 * Template Name: Contact Page
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package engage
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();
$container = get_theme_mod( 'engage_container_type' );

if ( is_front_page() ) {
	get_template_part( 'global-templates/hero' );
}
?>

<div class="wrapper" id="full-width-page-wrapper">
	<div class="container">
		<h1 class="slim-pagetitle mb-4"><?php the_title();?></h1>
		<?php the_content();?>
		<div class="row">
			<div class="col-md-4">
				<div class="card card-body pd-25 quick-contact">
		            <h6 class="slim-card-title mg-b-0">Quick Contact Form</h6>
		            <?php echo do_shortcode('[gravityform id="4" title="false" description="false" ajax="true" tabindex="49"]');?>
          		</div>
			</div>
			<div class="col-md-8">

				<?php $eventargs = array(
					    'posts_per_page' => -1,
					    'post_type' => 'team',
						'order'		=> 'ASC',
					);

					$eventquery = new WP_Query( $eventargs );
							 
						if ( $eventquery->have_posts() ) {

							echo '<div class="row">';
						 
						    while ( $eventquery->have_posts() ) {
						 
						        $eventquery->the_post();?>

						        <div class="col-sm-6 col-lg-4">
						        	 <div class="card-contact mb-4">
	                  					<div class="tx-center">
	                  						<?php if ( has_post_thumbnail() ) :?>
	                  							<?php echo the_post_thumbnail('thumbnail', array('class' => 'card-img')); ?>
                  							<?php else:?>
                  								<img class="card-img" src="<?php echo get_template_directory_uri(); ?>/assets/user-outline-placeholder.png">
                  							<?php endif;?>
	                  						<h5 class="mg-t-10 mg-b-5 tx-inverse"><?php the_title();?></h5>
	                  						<p><?php the_field('job_title');?></p>
	                  					</div>
	                  					<p class="contact-item">
							                <span>Phone:</span>
							                <span><?php the_field('phone');?></span>
							              </p><!-- contact-item -->
							              <p class="contact-item">
							            
							                <a class="tx-center" href="mailto:<?php the_field('email');?>"><?php the_field('email');?></a> 
							           
							              </p><!-- contact-item -->
	                  				 </div>
						       	</div>
						    <?php } ?>

						</div>

				        <?php } else { ?>

				<?php } wp_reset_postdata(); ?>
			</div>

		</div>
	</div>
</div><!-- #full-width-page-wrapper -->

<?php
get_footer();
