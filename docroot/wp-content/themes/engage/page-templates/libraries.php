<?php
/**
 * Template Name: libraries Page
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package engage
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();
$container = get_theme_mod( 'engage_container_type' );

if ( is_front_page() ) {
	get_template_part( 'global-templates/hero' );
}
?>

<div class="wrapper" id="full-width-page-wrapper">
	<div class="container">
		<h1 class="slim-pagetitle mb-4"><?php the_title();?></h1>
		<?php if ( ! get_field( 'libraries','options' ) ):?>
			<?php the_content();?>
			<div class="row">
				<?php if (   ! get_field('document_library','options') ) :?>
					<div class="col-md-6">
						<a href="/document-library/" title="view documentation">
							<div class="card card-blog">
					            <figure class="card-item-img bg-mantle">
					              <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets/documents.png">
					            </figure>
			            		<div class="card-body">
			          				<p class="blog-category">Documents</p>
					              	<h5 class="blog-title"><a href="">View all documents and presentations</a></h5>
					             	<p class="blog-text">Read through all the public documentation for the proposed development</p>
					            </div><!-- card-body -->
			          		</div>
			          	</a>
					</div>
				<?php endif;?>
				<?php if (   ! get_field('webinar_library','options') ) :?>
				<div class="col-md-6">
					<a href="/video-library/" title="view webinars">
						<div class="card card-blog">
				            <figure class="card-item-img bg-mantle">
				              <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets/webinar.png">
				            </figure>
		            		<div class="card-body">
		              		<p class="blog-category">Videos / Webinars</p>
				              <h5 class="blog-title"><a href="">Watch the latest webinars</a></h5>
				              <p class="blog-text">Watch video presentations regarding the proposed development</p>
				            </div><!-- card-body -->
		          		</div>
		          	</a>
				</div>
				<?php endif;?>
			</div>
		</div>
		<?php else:?>
			<p>These features are not currently active, please contact the site administrator to enquire about adding it.</p>
		<?php endif;?>
</div><!-- #full-width-page-wrapper -->

<?php
get_footer();
