<?php
/**
 * Template Name: About Page
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package engage
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();
$container = get_theme_mod( 'engage_container_type' );

if ( is_front_page() ) {
	get_template_part( 'global-templates/hero' );
}
?>

<div class="wrapper p-0" id="full-width-page-wrapper">
	<div class="container">
		<h1 class="slim-pagetitle mt-4"><?php the_title();?></h1>
		<div class="row mt-4 mb-4">
			<div class="col-md-8">
				<div class="section-wrapper tx-inverse">
					<?php the_content();?>
				</div>
			</div>
			<div class="col-md-4">
				<?php

					// Check rows exists.
					if( have_rows('images') ):

					    // Loop through rows.
					    while( have_rows('images') ) : the_row();?>

					        <?php 
								$image = get_sub_field('image');
								if( !empty( $image ) ): ?>
								    <img class="img-thumbnail mb-3" src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
							<?php endif; ?>

					    
					    <?php endwhile;

					// No value.
					else :
					    // Do something...
					endif;
					?>

			</div>
		</div>
	</div>
</div><!-- #full-width-page-wrapper -->

<?php
get_footer();
