<?php
/**
 * Template Name: Questionnaire Template
 *
 * This template can be used to override the default template and sidebar setup
 *
 * @package duckiee
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();
?>

<div class="wrapper" id="page-wrapper">

	<div class="container" id="content">

		<h1 class="slim-pagetitle mb-4"><?php the_title();?></h1>

		<?php the_content();?>

		<div class="row">
			


		<?php $eventargs = array(
			    'posts_per_page' => -1,
			    'post_type' => 'polls',
				'order'		=> 'ASC',
			);

			$eventquery = new WP_Query( $eventargs );
					 
				if ( $eventquery->have_posts() ) {
				 
				    while ( $eventquery->have_posts() ) {
				 
				        $eventquery->the_post();?>

				        <a href="<?php if( get_field('standalone_url') ): ?><?php the_field('standalone_url');?><?php else:?><?php the_permalink();?><?php endif;?>" class="col-md-4" title="<?php the_title();?>">
							<div class="teaser">
								<div class="image-container">
									<?php echo the_post_thumbnail();?>
									<div class="overlay">
									  <div class="text">Start Poll <i class="far fa-arrow-alt-circle-right"></i></div>
									</div>
								</div>
								<div class="teaser-content">
									<h4><?php the_title();?></h4>
									<div class="arrow">Answer Poll <i class="far fa-arrow-alt-circle-right"></i></div>
								</div>
							</div>
						</a>
				        
				    <?php } ?>

		        <?php } else { ?>
				
			<?php } wp_reset_postdata(); ?>

		</div>

	</div><!-- #content -->

</div><!-- #page-wrapper -->

<?php
get_footer();

