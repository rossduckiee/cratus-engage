<?php
/**
 * Template Name: Full Width Page
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package engage
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();
$container = get_theme_mod( 'engage_container_type' );

if ( is_front_page() ) {
	get_template_part( 'global-templates/hero' );
}
?>

<div class="wrapper p-0" id="full-width-page-wrapper">
	<?php if( get_field('cta') ): ?>
		<div class="question-cta">
			<div class="container">
				<h3><?php the_title();?></h3>
				<?php the_field('cta');?>
				<?php $url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
				if (strpos($url,'virtual-consultation') !== false) {
				echo do_shortcode('[homebutton]');
				}else{
					echo do_shortcode('[buttons]');
				}?>
			</div>
		</div>
	<?php endif;?>
	<div class="full-width-section <?php if ( get_field( 'scrollable' ) ): ?>h-scrollable<?php endif;?>">
		<?php the_field('full_width_section');?>
		<!-- // SlideOut -->
		<?php $url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
				if (strpos($url,'virtual-consultation') !== false) :?>
					<div id="slideOut">
					<!--   // Tab -->
					  <div class="slideOutTab">
					    <div>
					      <p>Leave Feedback</p>
					    </div>
					  </div>
					  <div class="modal-content">
					    <div class="modal-header">
					      <h4 class="modal-title">Leave Feedback</h4> 
					  	</div>
					    <div class="modal-body">
					      <div class="email-container">
					      	<?php echo do_shortcode('[gravityform id="7" title="false" description="false" ajax="true" tabindex="49"]'); ?>
					      </div>
					    </div>
					  </div>
					</div>
				<?php endif;?>
		<div id="foo" class="modal fade show" style="display: block;">
	      <div class="modal-dialog modal-dialog-vertical-center" role="document">
	        <div class="modal-content bd-0 tx-14">
	          <div class="modal-header">
	            <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><?php the_title();?></h6>
	            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	              <span aria-hidden="true">×</span>
	            </button>
	          </div>
	          <div class="modal-body pd-25">
	            <h5 class="lh-3 mg-b-20"><a href="" class="tx-inverse hover-primary">Getting Started</a></h5>
	            <p class="mg-b-5"><?php the_content();?></p>
	          </div>
	          <div class="modal-footer">
	            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	          </div>
	        </div>
	      </div><!-- modal-dialog -->
	    </div>
	</div>
</div><!-- #full-width-page-wrapper -->
<?php
get_footer();
