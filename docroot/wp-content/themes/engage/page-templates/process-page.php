<?php
/**
 * Template Name: Process (timeline) Template
 *
 * This template can be used to override the default template and sidebar setup
 *
 * @package duckiee
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();
?>

<div class="wrapper" id="page-wrapper">

	<div class="container" id="content">

	<h1 class="slim-pagetitle mb-4"><?php the_title();?></h1>

	<?php the_content();?>

	

	<div class="row row-sm row-timeline mb-5">
          <div class="col-lg-12">

            <div class="card pd-30">
              <div class="timeline-group">
                <div class="timeline-item timeline-day">
                  <div class="timeline-time">&nbsp;</div>
                  <div class="timeline-body">
                    
                  </div><!-- timeline-body -->
                </div><!-- timeline-item -->
                <?php

				// Check rows exists.
				if( have_rows('timeline') ):

				    // Loop through rows.
				    while( have_rows('timeline') ) : the_row(); ?>

				    	<div class="timeline-item <?php if ( get_sub_field( 'disable' ) ): ?>disable<?php endif;?>">
				    		 <div class="timeline-time"><?php the_sub_field('date');?></div>
				    		 <div class="timeline-body">
				    		 	<p class="timeline-title"><?php the_sub_field('title');?></p>
				    		 	<p class="timeline-author"><?php the_sub_field('status');?></p>
				    		 	<p class="timeline-text"><?php the_sub_field('content');?></p>
				    		 </div>
				    	</div>
				       
				    <?php endwhile;

				// No value.
				else :
				    // Do something...
				endif; ?>
              </div><!-- timeline-group -->
            </div><!-- card -->
        </div>
    </div>

</div><!-- #page-wrapper -->

<?php
get_footer();

