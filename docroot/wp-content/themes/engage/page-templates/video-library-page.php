<?php
/**
 * Template Name: Video Library Page
 *
 * Template for displaying a documents
 * @package engage
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();
$container = get_theme_mod( 'engage_container_type' );
?>

<div class="wrapper" id="full-width-page-wrapper">
	<div class="container">
		<?php the_content();?>
		<div class="row">
			<div class="col-md-12 mb-3">
				<h1 class="slim-pagetitle mb-4"><?php the_title();?></h1>
			</div>
			<div class="col-md-4 side-nav">
				<h5 class="tx-inverse mb-3">Quick Links</h5>
				<div class="quick-filters nav">
				<?php if (   ! get_field('webinar_library','options') ) :?>
					<a href="/video-library" class="nav-link active">
	                	<span>Webinars / Videos</span>
	              	</a>
              	<?php endif;?>
              	<?php if (   ! get_field('document_library','options') ) :?>
		              <a href="/document-library" class="nav-link">
		                <span>Documents</span>
		              </a>
		        <?php endif;?>
              <a href="/polls" class="nav-link">
                <span>Feedback</span>
              </a>
              <?php if ( ! get_field( 'timeline','options' ) ): ?>
	              <a href="/process" class="nav-link">
	                <span>Timeline</span>
	              </a>
	          <?php endif;?>
	          <?php if ( ! get_field( 'latest_news','options' ) ): ?>
	              <a href="/news-updates" class="nav-link">
	                <span>Latest News & Updates</span>
	              </a>
	          <?php endif;?>
            </div>
			
			</div>
			<div class="col-md-8">
				<div class='row'>
					<?php $eventargs = array(
						    'posts_per_page' => -1,
						    'post_type' => 'videos',
							'order'		=> 'ASC',
						);

						$eventquery = new WP_Query( $eventargs );
								 
							if ( $eventquery->have_posts() ) {
							 
							    while ( $eventquery->have_posts() ) {
							 
							        $eventquery->the_post();?>
							       	<div class="col-md-6">

								     	<a class="section-teasers" href="<?php the_permalink();?>">
									     	<div class="image-container home-teasers">
									     		<?php echo get_the_post_thumbnail();?>
									     	</div>
									     	<div class="lower-teaser-content">
									     		<h4><?php the_title();?></h4>
									     		<div class="icon-click">
									     			<img src="<?php echo get_template_directory_uri(); ?>/assets/chevron.svg">
									     		</div>
									     	</div>
									    </a>
									</div>
							    <?php } ?>

					        <?php } else { ?>
							
						<?php } wp_reset_postdata(); ?>
					</div>
				</div>
		</div>
	</div>
</div><!-- #full-width-page-wrapper -->

<?php
get_footer();
