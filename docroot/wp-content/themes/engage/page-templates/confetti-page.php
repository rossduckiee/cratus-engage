<?php
/**
 * Template Name: Confetti Page
 *
 * Template for displaying a page with confetti for form completion
 *
 * @package engage
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();
$container = get_theme_mod( 'engage_container_type' );

if ( is_front_page() ) {
	get_template_part( 'global-templates/hero' );
}
?>

<?php $siteurl = urlencode(get_site_url());?>
<?php $sharemessage = urlencode(get_field('share_message'));?>
<div class="wrapper p-0" id="full-width-page-wrapper">
	<div class="confetti-background">
		<div id="poll-modal" class="modal fade show position-relative" style="display: block;">
		  <div class="modal-dialog modal-lg" role="document">
		    <div class="modal-content tx-size-sm">
		    	<div class="header-region">
		      		<svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52"><circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none"/><path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8"/></svg>
		       	 <h1 class="tx-14 mg-b-0 tx-uppercase tx-bold text-center w-100"><?php the_title();?></h1>
		       	</div>
		      <div class="modal-body pd-20 w-100 text-center ">
		       <?php the_content();?>
		       <hr>
		       <h2>Spread the word and the power.</h2>
		       <ul class="share-buttons">
				<li><a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $siteurl;?>%2F&quote=<?php echo $sharemessage;?>" target="_blank" title="Share on Facebook"><img alt="Icon of a computer" class="d-block" src="<?php echo get_template_directory_uri(); ?>/assets/facebook.svg"><span class="sr-only">Share on Facebook</span></a></li>
				<li><a href="https://twitter.com/intent/tweet?source=<?php echo $siteurl;?>%2F&text=<?php echo $sharemessage;?>:%20<?php echo $siteurl;?>%2F" target="_blank" title="Tweet"><img alt="Icon of a computer" class="d-block" src="<?php echo get_template_directory_uri(); ?>/assets/twitter.svg"><span class="sr-only">Tweet</span></a></li>
				</ul>
		       <hr>
		      <div class="button-cta-shortcode d-flex mt-3 mb-4 justify-content-center">
                    <a class="btn btn-primary" href="/">Back to main site <i class="fa fa-angle-right mg-l-5"></i></a>
                </div>
		      </div><!-- modal-body -->
		    </div>
		  </div><!-- modal-dialog -->
		</div>
	</div>
</div><!-- #full-width-page-wrapper -->

<?php
get_footer();
