<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package duckiee
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();

?>

<div class="pb-5">
      <div class="container">
        <div class="slim-pageheader">
          <ol class="breadcrumb slim-breadcrumb">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Engage Dashboard</li>
          </ol>
          <h6 class="slim-pagetitle"><?php the_title();?></h6>
        </div><!-- slim-pageheader -->

        <div class="dash-headline">
          <div class="dash-headline-left">
            <div class="dash-headline-item-one">
              <div id="" class=""></div>
              <div class="dash-item-overlay">
                <h1><?php the_field('large_title');?></h1>
                <p class="earning-label"><?php the_field('large_subtitle');?></p>
                <p class="earning-desc"><?php the_field('large_description');?></p>
                <div class="d-flex">
                <a href="<?php the_field('large_link');?>" class="statement-link mr-3"><?php the_field('large_link_text');?><i class="fa fa-angle-right mg-l-5"></i></a><a href="http://cratusengage.co.uk/virtual-consultation-3d/" target="_blank" class="statement-link">Enter 3D<i class="fa fa-angle-right mg-l-5"></i></a>
                </div>
                <img alt="network image with virtual style" src="<?php echo get_template_directory_uri(); ?>/assets/network-corner.png">
              </div>
            </div><!-- dash-headline-item-one -->
          </div><!-- dash-headline-left -->

          <div class="dash-headline-right">
            <div class="dash-headline-right-top">
              <div class="dash-headline-item-two">
              	<div class="logo-container-home">
              		<?php the_custom_logo();?>
              	</div>
                <div class="dash-item-overlay">
                  <h4><?php the_field('project_title');?></h4>
                  <p class="item-label"><?php the_field('project_client');?></p>
                  <p class="item-desc"><?php the_field('project_description');?></p>
                  <a href="/about" class="report-link"><?php the_field('project_link_text');?> <i class="fa fa-angle-right mg-l-5"></i></a>
                </div>
              </div><!-- dash-headline-item-two -->
            </div><!-- dash-headline-right-top -->
            <div class="dash-headline-right-bottom">
              <div class="dash-headline-right-bottom-left">
	                <div class="dash-headline-item-three">
	                  <div>
	                    <a href="/polls">
                          <h3 class="title-link"><?php the_field('focus_left_title');?></h3>
                        </a>
                        <p class="item-desc"><?php the_field('focus_left_description');?></p>
                        <a href="/polls" class="polls_link">Give your feedback <i class="fa fa-angle-right mg-l-5"></i></a>
	                  </div>
	                </div><!-- dash-headline-item-three -->
              </div><!-- dash-headline-right-bottom-left -->
              <div class="dash-headline-right-bottom-right">
	                <div class="dash-headline-item-three">
	                  <div>
                        <a href="/libraries"> 
	                        <h3 class="title-link"><?php the_field('focus_right_title');?></h3>
	                    </a>
                        <p class="item-desc"><?php the_field('focus_right_description');?></p>
                        <a href="/libraries" class="library_link">Learn More  <i class="fa fa-angle-right mg-l-5"></i></a>  
	                  </div>
	                </div><!-- dash-headline-item-three -->
              </div><!-- dash-headline-right-bottom-right -->
            </div><!-- dash-headline-right-bottom -->
          </div><!-- wd-50p -->
        </div><!-- d-flex ht-100v -->
        <?php $boxcount = 0;?>
        <?php if ( ! get_field( 'timeline','options' ) ): ?>
          <?php $boxcount++;?>
        <?php endif;?>
         <?php if ( ! get_field( 'ideas_wall','options' ) ): ?>
          <?php $boxcount++;?>
        <?php endif;?>
         <?php if ( ! get_field( 'latest_news','options' ) ): ?>
          <?php $boxcount++;?>
        <?php endif;?>
         <?php if ( ! get_field( 'contact','options' ) ): ?>
          <?php $boxcount++;?>
        <?php endif;?>
        <?php $colcount = 12 / $boxcount;?>
        
        <div class="card card-dash-one mg-t-20 mb-4">
          <div class="row no-gutters">
            <?php if ( ! get_field( 'timeline','options' ) ): ?>
              <div class="col-lg-<?php echo $colcount;?>">
              <a class="quick-link-wrapper" href="<?php the_field('box_1_link');?>">
  	        	<i class="icon ion-ios-clock-outline"></i>
  	              <div class="dash-content">
  	                <label class="tx-primary"><?php the_field('box_1_subtitle');?></label>
  	                <h2><?php the_field('box_1_title');?></h2>
  	              </div><!-- dash-content -->
  	          </a>
              </div><!-- col-3 -->
            <?php endif;?>
            <?php if ( ! get_field( 'ideas_wall','options' ) ): ?>
              <div class="col-lg-<?php echo $colcount;?>">
      		      <a class="quick-link-wrapper" href="<?php the_field('box_2_link');?>">
  	              <i class="icon ion-ios-chatboxes-outline"></i>
  	              <div class="dash-content">
  	                <label class="tx-success"><?php the_field('box_2_subtitle');?></label>
  	                <h2><?php the_field('box_2_title');?></h2>
  	              </div><!-- dash-content -->
  	            </a>
              </div><!-- col-3 -->
            <?php endif;?>
            <?php if ( ! get_field( 'latest_news','options' ) ): ?>
              <div class="col-lg-<?php echo $colcount;?>">
                <a class="quick-link-wrapper" href="<?php the_field('box_3_link');?>">
  	    		       <i class="icon ion-ios-bolt-outline"></i>
  	              <div class="dash-content">
  	                <label class="tx-purple"><?php the_field('box_3_subtitle');?></label>
  	                <h2><?php the_field('box_3_title');?></h2>
  	              </div><!-- dash-content -->
  	             </a>
              </div><!-- col-3 -->
            <?php endif;?>
            <?php if ( ! get_field( 'contact','options' ) ): ?>
              <div class="col-lg-<?php echo $colcount;?>">
              	<a class="quick-link-wrapper" href="<?php the_field('box_4_link');?>">
  	              <i class="icon ion-ios-email-outline"></i>
  	              <div class="dash-content">
  	                <label class="tx-danger"><?php the_field('box_4_subtitle');?></label>
  	                <h2><?php the_field('box_4_title');?></h2>
  	              </div><!-- dash-content -->
  	          	</a>
              </div><!-- col-3 -->
            <?php endif;?>
          </div><!-- row -->
        </div><!-- card -->
    </div>
</div>

<?php
get_footer();
