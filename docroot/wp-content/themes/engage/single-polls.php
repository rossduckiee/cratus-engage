<?php
/**
 * The template for displaying all single posts
 *
 * @package engage
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();
$container = get_theme_mod( 'engage_container_type' );
?>

<div class="wrapper" id="single-wrapper">

	<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

		<div class="row">

			<!-- Do the left sidebar check -->
			<?php get_template_part( 'global-templates/left-sidebar-check' ); ?>

			<main class="site-main" id="main">
				
				<?php
				while ( have_posts() ) {
					the_post(); ?>

					<div id="poll-modal" class="modal fade show position-relative" style="display: block;">
					  <div class="modal-dialog modal-lg" role="document">
					    <div class="modal-content tx-size-sm">
					      <div class="modal-header pd-x-20">
					        <h1 class="tx-14 mg-b-0 tx-uppercase tx-bold"><?php the_title();?></h1>
					      </div>
					      <div class="modal-body pd-20">
					       <?php the_content();?>
					      </div><!-- modal-body -->
					    </div>
					  </div><!-- modal-dialog -->
					</div>
					
				<?php } ?>

			</main><!-- #main -->

			<!-- Do the right sidebar check -->
			<?php get_template_part( 'global-templates/right-sidebar-check' ); ?>

		</div><!-- .row -->

	</div><!-- #content -->

</div><!-- #single-wrapper -->

<?php
get_footer();
