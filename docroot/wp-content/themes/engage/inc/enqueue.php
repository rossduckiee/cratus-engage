<?php
/**
 * engage enqueue scripts
 *
 * @package engage
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

if ( ! function_exists( 'engage_scripts' ) ) {
	/**
	 * Load theme's JavaScript and CSS sources.
	 */
	function engage_scripts() {
		// Get the theme data.
		$the_theme     = wp_get_theme();
		$theme_version = $the_theme->get( 'Version' );

		$css_version = $theme_version . '.' . filemtime( get_template_directory() . '/css/theme.min.css' );
		wp_enqueue_style( 'engage-ui-styles', get_template_directory_uri() . '/template/app/css/slim.min.css', array() );
		wp_enqueue_style( 'engage-styles', get_template_directory_uri() . '/css/theme.min.css', array(), $css_version );
		wp_enqueue_style( 'engage-ui-fa', get_template_directory_uri() . '/template/app/lib/font-awesome/css/font-awesome.css', array() );
		wp_enqueue_style( 'engage-ui-ion', get_template_directory_uri() . '/template/app/lib/Ionicons/css/ionicons.css', array() );
		wp_enqueue_style( 'engage-font-gibson', 'https://use.typekit.net/ohv0wbq.css', array() );

		$js_version = $theme_version . '.' . filemtime( get_template_directory() . '/js/theme.min.js' );
		wp_enqueue_script( 'jquery' );
		wp_enqueue_script( 'cookie', 'https://cdn.jsdelivr.net/npm/js-cookie@rc/dist/js.cookie.min.js', null, null, true );
		wp_enqueue_script( 'engage-scripts', get_template_directory_uri() . '/js/theme.min.js', array(), $js_version, true );
  
		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}
	}
} // End of if function_exists( 'engage_scripts' ).

add_action( 'wp_enqueue_scripts', 'engage_scripts' );
