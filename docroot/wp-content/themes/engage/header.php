<?php
/**
 * The header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package engage
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

$container = get_theme_mod( 'engage_container_type' );
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-config" content="/wp-content/themes/engage/assets/favicons/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
    <link rel="apple-touch-icon" sizes="180x180" href="/wp-content/themes/engage/assets/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/wp-content/themes/engage/assets/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/wp-content/themes/engage/assets/favicons/favicon-16x16.png">
    <link rel="manifest" href="/wp-content/themes/engage/assets/favicons/site.webmanifest">
    <link rel="mask-icon" href="/wp-content/themes/engage/assets/favicons/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="/wp-content/themes/engage/assets/favicons/favicon.ico">   
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?> <?php engage_body_attributes(); ?>>
<div id="cover"></div>
<?php do_action( 'wp_body_open' ); ?>
<nav class="menu-left">
  <div id="main-nav" aria-labelledby="main-nav-label">

	<h2 id="main-nav-label" class="sr-only">
		<?php esc_html_e( 'Main Navigation', 'engage' ); ?>
	</h2>
	<h2 class="slim-logo"><a title="home" href="/">Engage</a></h2>

	<div class="row w-100">
				<div class="col-12">
					<?php
						wp_nav_menu(
							array(
								'theme_location'  => 'primary',
								'container_class' => '',
								'container_id'    => '',
								'menu_class'      => '',
								'fallback_cb'     => '',
								'menu_id'         => '',
								'depth'           => 2,
								'walker'          => new engage_WP_Bootstrap_Navwalker(),
							)
						);
						?>
				</div>
			</div>
		</div>

			
</nav>
<div class="header-cta">
	<p class="mb-0">This site has been created as a demonstration, the content contained within is fictional and for illustrative purposes only.</p>
</div>
<div class="site menu-push" id="page">
<?php if(! is_page('welcome')):?>
		<div class="slim-header">
	      <div class="container">
	        <div class="slim-header-left">
	          <h2 class="slim-logo"><a title="home" href="/">Engage</a></h2>
	          <?php get_search_form(); ?>
	        </div><!-- slim-header-left -->
	        <div class="slim-header-right">    
	          <div class="dropdown dropdown-c">
	            <div id="navbar-toggle"><label for="sidebartoggler" class="toggle"><span class="sr-only">Navigation Toggle</span><div class="hamb"></div><div class="hamb" style=""></div><div class="hamb"></div></label><img alt="close icon" class="close-icon" src="<?php echo get_template_directory_uri(); ?>/assets/close.svg"></div>
	            
	          </div><!-- dropdown -->
	        </div><!-- header-right -->
	      </div><!-- container -->
    	</div><!-- slim-header -->
	<?php endif;?>

