# config valid for current version and patch releases of Capistrano
lock "~> 3.14.1"

set :application, "engage"
set :repo_url, "git@bitbucket.org:rossduckiee/engage.git"
set :deploy_to, '/home3/engage/capistrano'
set :tmp_dir, '/home3/engage/tmp'
# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# Default deploy_to directory is /var/www/my_app_name
# set :deploy_to, "/var/www/my_app_name"

set :repo_tree, 'docroot'

# Default value for :format is :airbrussh.
# set :format, :airbrussh

# You can configure the Airbrussh format using :format_options.
# These are the defaults.
# set :format_options, command_output: true, log_file: "log/capistrano.log", color: :auto, truncate: :auto

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
set :linked_files, fetch(:linked_files, []).push('.htaccess', 'wp-config.php')

# Default value for linked_dirs is []
set :linked_dirs, fetch(:linked_dirs, []).push('wp-content/uploads')

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for local_user is ENV['USER']
# set :local_user, -> { `git config user.name`.chomp }

# Default value for keep_releases is 5
set :keep_releases, 1

# Uncomment the following to require manually verifying the host key before first deploy.
# set :ssh_options, verify_host_key: :secure

namespace :deploy do

  # @source https://gist.github.com/rsutphin/9010923
  after :finishing, :tag_and_push_tag do
    on roles(:app) do
      within release_path do
        set(:current_revision, capture(:cat, 'REVISION'))

        # release path may be resolved already or not
        resolved_release_path = capture(:pwd, "-P")
        set(:release_name, resolved_release_path.split('/').last)
      end
    end

    run_locally do
      user = capture(:git, "config --get user.name")
      email = capture(:git, "config --get user.email")
      tag_msg = "Deployed by #{user} <#{email}> to #{fetch :stage} as #{fetch :release_name}"

      tag_name = "#{fetch :stage }-#{fetch :release_name}"
      execute :git, %(tag #{tag_name} #{fetch :current_revision} -m "#{tag_msg}")
      execute :git, "push --tags origin"
    end
  end

end
